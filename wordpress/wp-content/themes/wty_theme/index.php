<!DOCTYPE html>
<html lang="ru">

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <script src="https://code.jquery.com/jquery-3.3.1.slim.min.js"
        integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo"
        crossorigin="anonymous"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.3/umd/popper.min.js"
        integrity="sha384-ZMP7rVo3mIykV+2+9J3UJ46jBk0WLaUAdn689aCwoqbBJiSnjAK/l8WvCWPIPm49"
        crossorigin="anonymous"></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/js/bootstrap.min.js" integrity="sha384-ChfqqxuZUCnJSK3+MXmPNIyE6ZbWh2IMqE241rYiqJxyMiZ6OW/JmZQ5stwEULTy" crossorigin="anonymous"></script>
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css" integrity="sha384-MCw98/SFnGE8fJT3GXwEOngsV7Zt27NXFoaoApmYm81iuXoPkFOJwJ8ERdknLPMO" crossorigin="anonymous">

    <?php

    // Этот метод вызывает функцию инициализации импортов из папки functions.php
    wp_head()

    ?>

    <title>ПобедаВместеЯмал</title>
</head>

<body>
    <div class="wrapper">
        <?php get_header() ?>
        <main>
            <wpforms id="13" title="false">

            </wpforms>
        <div class="modal fade" id="ModalForm" tabindex="-1" role="dialog" aria-hidden="true">
                <div class="modal-dialog modal-dialog-centered" role="document">
                  <div class="modal-content card-form card-form__text ">
                    <div class="form-about__title justify-content-center">
                    <img class="mr-3" src="<?php echo bloginfo("template_url"); ?>/assets/img/logo.png" alt="logo" class="logo">
                      <h2 class="m-0">Форма подачи заявки</h2>
                    </div>
                    <form method="post" enctype="multipart/form-data" class="form js-form-validate">
                      <div class="form-row">
                        <div class="form-group col form-group__margin">
                          <label for="fio">ФИО участника*</label>
                          <input type="text" id="fio" name="fio" class="form-control" placeholder="Начните вводить">
                        </div>
                        <div class="form-group col-5">
                          <label for="birthDate">Год рождения*</label>
                          <input type="date" id="datepicker" name="datepicker" id="birthDate" class="form-control">
                        </div>
                        
                      </div>
                      <div class="form-group">
                        <label for="address">Домашний адрес*</label>
                        <input type="text" id="address" name="address" class="form-control" placeholder="Начните вводить">
                      </div>
                      <div class="form-row">
                        <div class="form-group col form-group__margin">
                          <label for="email">Адрес электронной почты*</label>
                          <input type="email" id="email" name="email" class="form-control" placeholder="Начните вводить">
                        </div>
                        <div class="form-group col-5">
                          <label for="phone">Телефон*</label>
                          <input type="phone" id="phone" name="phone" class="form-control" placeholder="+7 ___-___-__-__">
                        </div>
                      </div>
                      <div class="form-group">
                        <label for="event">Название акции, в которой вы желаете принять участие*</label>
                        <select id="event" name="event" class="form-control">
                            <option selected disabled>Нажмите, чтобы выбрать</option>
                            <option value="Книга памяти Ямал">Книга памяти Ямал</option>
                            <option value="История проходит через дом">История проходит через дом</option>
                            <option value="Шарф Победы">Шарф Победы</option>
                            <option value="Письма войны">Письма войны</option>
                            <option value="Мы помним">Мы помним</option>
                        </select>
                      </div>
                      <div class="form-group">
                        <label for="city">Город*</label>
                        <select id="city" name="city" class="form-control">
                            <option selected disabled>Нажмите, чтобы выбрать</option>
                            <option>Аксарка</option>
                            <option>Губкинский</option>
                            <option>Лабытнанги</option>
                            <option>Муравленко</option>
                            <option>Надым</option>
                            <option>Ноябрьск</option>
                            <option>Салехард</option>
                            <option>Тазовский</option>
                            <option>Тарко-Сале</option>
                            <option>Ханымей</option>
                        </select>
                      </div>
                      <div id="formDisplay" class="display-switcher">
                        <div id="biographyDisplay" class="form-group  display-switcher">
                            <label for="biography">Биографические данные ветерана Великой Отечественной войны*</label>
                            <textarea type="text" id="biography" rows="3" name="biography" class="form-control form-control__start" placeholder="ФИО, годы жизни, звание, род войск"></textarea>
                        </div>
                        <div id="combatPathDisplay" class="form-group display-switcher">
                            <label for="combatPath">Информация о боевом пути ветерана*</label>
                            <textarea type="text" rows="6" id="combatPath" name="combatPath" class="form-control form-control__start" placeholder="Начните вводить"></textarea>
                        </div>
                        <label>Поле вставки изображений*</label>
                        <div class="form-group">
                            <label id="labelForImages" class="form-control form-control__images" for="images">Нажмите, чтобы выбрать файлы (Поддерживаемые форматы: JPG, PNG)</label>
                            <input type="file" id="images" name="images"  placeholder="Нажмите, чтобы выбрать файлы (Поддерживаемые форматы: JPG, PNG)" multiple>
                        </div>
                      </div>
                      <div class="form-group mb-5">
                        <label class=" custom-control custom-checkbox">
                            <input type="checkbox" name="checker" id="checker" class="custom-control-input">
                            <span class="custom-control-indicator"></span>
                            <span class="custom-control-label">Я принимаю условия политики конфиденциальности и даю согласие на обработку персональных данных</span>
                        </label>
                      </div>
                      <div class="d-flex justify-content-center mb-3">
                        <input id="formSubmit" type="submit" value="Отправить заявку" class="btn btn-submit">
                      </div>
                      <div class="d-flex justify-content-center">
                        <a class="form-link" href="#">Подробности о подаче заявки</a>
                      </div>
                    </form>
                  </div>
                </div>
            </div>
            <DIV class="container">
                <div class="about">
                    <div class="about__image col-3">
                        <img src="<?php echo bloginfo("template_url"); ?>/assets/img/aboutProject.png" alt="">
                    </div>
                    <div class="about__content col-4">
                        <div class="about__title">
                            <h2>О проекте</h2>
                        </div>
                        <div class="about__text">Проект #ПобедаВместеЯмал посвящен празднованию 76-й годовщины Победы в
                            Великой Отечественной войне 1941-1945 годов и направлен на сохранение исторической памяти о
                            героическом подвиге советского народа в период войны.</div>
                    </div>
                </div>
                <div class="action">
                    <div class="action__title ">
                        <h2>Акции проекта</h2>
                    </div>
                    <div class="action__items ">
                        <a href="<?php echo home_url(); ?>/event">
                            <div class="action__items-card card col-3"><img src="<?php echo bloginfo("template_url"); ?>/assets/img/MemoryBook.png" alt="">
                                <div class="card__title">Книга памяти</div>
                                <div class="card__text">подробнее</div>
                            </div>
                        </a>
                        <a href="<?php echo home_url(); ?>/event">
                            <div class="action__items-card card col-3"><img src="<?php echo bloginfo("template_url"); ?>/assets/img/HomeHistory.png" alt="">
                                <div class="card__title">История проходит через дом</div>
                                <div class="card__text">подробнее</div>
                            </div>
                        </a>
                        <a href="<?php echo home_url(); ?>/event">
                            <div class="action__items-card card col-3"><img src="<?php echo bloginfo("template_url"); ?>/assets/img/Scarf.png" alt="">
                                <div class="card__title">Шарф победы</div>
                                <div class="card__text">подробнее</div>
                            </div>
                        </a>
                        <a href="<?php echo home_url(); ?>/event">
                            <div class="action__items-card card col-3"><img src="<?php echo bloginfo("template_url"); ?>/assets/img/Letter.png" alt="">
                                <div class="card__title">Письма войны</div>
                                <div class="card__text">подробнее</div>
                            </div>
                        </a>
                        <a href="<?php echo home_url(); ?>/eventl">
                            <div class="action__items-card card col-3"><img src="<?php echo bloginfo("template_url"); ?>/assets/img/WeRemember.png" alt="">
                                <div class="card__title">Мы помним</div>
                                <div class="card__text">подробнее</div>
                            </div>
                        </a>
                    </div>
                </div>
            </div>
        </main>
        <?php get_footer() ?>
    </div>

    <?php wp_footer() ?>
</body>

</html>
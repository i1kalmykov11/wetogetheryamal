<footer>
    <div class="container">
        <div class="footer">
            <div class="footer__museums col-5">
                <div class="footer__title">Музеи</div>
                <ul class="footer__list list">
                    <li class="list-item">МБУ Губкинский музей освоения Севера </li>
                    <li class="list-item">МБУК Ханымейский историко-краеведческий музей</li>
                    <li class="list-item">МАУК Городской краеведческий музей</li>
                    <li class="list-item">ГБУ Ямало-Ненецкий окружной музейно-выставочный комплекс имени И.С.
                        Шемановского</li>
                </ul>
            </div>
            <div class="footer__contacts col-3">
                <div class="footer__title">Контакты</div>
                <ul class="footer__list list">
                    <li class="list-item">vk.com/pobedavnesteyamal</li>
                    <li class="list-item">@pobedavmesteyamal</li>
                    <li class="list-item">ok.ru/pobedavmesteyamal</li>
                    <li class="list-item">museum-noyabrsk@yandex.ru</li>
                </ul>
            </div>
        </div>
    </div>
</footer>
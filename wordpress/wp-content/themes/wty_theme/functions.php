<?php

add_action('wp_enqueue_scripts', 'wty_scripts');

function wty_scripts()
{
    wp_enqueue_style('main_styles', get_template_directory_uri() . '/assets/css/style.css?1');
    wp_enqueue_script('main_js', get_template_directory_uri() . '/assets/js/main.js?2', array(), NULL, true); // В файле main js помещаем все функции с логикой сайта
}

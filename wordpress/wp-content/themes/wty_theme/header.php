<header>
    <div class="header__up">
        <div class="container ">
            <h1>#ПобедаВместеЯмал</h1>
        </div>
    </div>
    <div class="header__down">
        <nav class="navbar navbar-expand-lg ">
            <div class="container">
                <a class="navbar-brand" href="<?php echo home_url();?>/index">
                    <img src="<?php echo bloginfo("template_url"); ?>/assets/img/logo.png" alt="logo" class="logo">
                </a>
                <div class="collapse navbar-collapse col-9" id="navbarNav">
                    <ul class="navbar-items navbar-nav">
                        <li class="nav-item" style="padding-left: 0px;">
                            <a class="nav-link" aria-current="page" href="<?php echo home_url(); ?>/event">Книга памяти Ямал</a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link" href="<?php echo home_url(); ?>/event">История проходит через дом</a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link" href="<?php echo home_url(); ?>/event">Шарф Победы</a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link" href="<?php echo home_url(); ?>/event">Письма войны</a>
                        </li>
                        <li class="nav-item" style="padding-right: 0px;">
                            <a class="nav-link" href="<?php echo home_url(); ?>/event">Мы помним</a>
                        </li>
                    </ul>
                </div>
                <button type="button" class="main-button col-2" data-toggle="modal" data-target="#ModalForm">
                    <span>Принять участие</span>
                </button>
            </div>
        </nav>
    </div>
</header>
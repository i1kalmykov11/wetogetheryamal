<?php

// Template Name: event

?>

<!DOCTYPE html>
<html lang="ru">

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <script src="https://code.jquery.com/jquery-3.3.1.slim.min.js"
        integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo"
        crossorigin="anonymous"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.3/umd/popper.min.js"
        integrity="sha384-ZMP7rVo3mIykV+2+9J3UJ46jBk0WLaUAdn689aCwoqbBJiSnjAK/l8WvCWPIPm49"
        crossorigin="anonymous"></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/js/bootstrap.min.js" integrity="sha384-ChfqqxuZUCnJSK3+MXmPNIyE6ZbWh2IMqE241rYiqJxyMiZ6OW/JmZQ5stwEULTy" crossorigin="anonymous"></script>
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css" integrity="sha384-MCw98/SFnGE8fJT3GXwEOngsV7Zt27NXFoaoApmYm81iuXoPkFOJwJ8ERdknLPMO" crossorigin="anonymous">
    <title>ПобедаВместеЯмал</title>

    <?php wp_head() ?>

</head>

<body>
    <div class="wrapper">

        <?php get_header() ?>

        <main>
            <div class="container">
                <div class="action__title" style="margin: 82px auto;">
                    <h2>Книга памяти Ямал</h2>
                </div>
            </div>
            <div class="event">
                <div class="col-md-2">
                    <img class="event__img" src="<?php echo bloginfo("template_url"); ?>/assets/img/MemoryBook.png" alt="" />
                </div>
                <div class="col-md-3">
                    <div class="event__title">
                        <h2>Описание</h2>
                    </div>
                    <div class="event__text">
                        Акция проекта направлена на c6op сведений об участниках Великой Отечественной войны,
                        призванных
                        с територии Ямала или проживавших/проживающих на территории Ямало-Ненецкого автономного
                        округа.
                        Расскажите о ветеране, историю его боевого пути и прикрепите фотографию к заявке.
                    </div>
                    <div class="event__buttons">
                        <button class="detailed-button" style="margin-right: 15px;">
                            <span>
                                Подробнее
                            </span>
                        </button>
                        <button class="main-button">
                            <span>
                                Принять участие
                            </span>
                        </button>
                    </div>
                </div>
            </div>
            <DIV class="container">
                <div class="action" style="margin-top: 0px;">
                    <div class="action__title ">
                        <h2>Альбомы городов</h2>
                    </div>
                    <div class="action__items ">
                        <a href="./event_page.html">
                            <div class="action__items-card card col-3"><img src="<?php echo bloginfo("template_url"); ?>/assets/img/Aksarka.png" alt="">
                                <div class="card__title">Аксарка</div>
                                <div class="card__text">посмотреть</div>
                            </div>
                        </a>
                        <a href="./event_page.html">
                            <div class="action__items-card card col-3"><img src="<?php echo bloginfo("template_url"); ?>/assets/img/Gubkinskii.png" alt="">
                                <div class="card__title">Губкинский</div>
                                <div class="card__text">посмотреть</div>
                            </div>
                        </a>
                        <a href="./event_page.html">
                            <div class="action__items-card card col-3"><img src="<?php echo bloginfo("template_url"); ?>/assets/img/Labintangi.png" alt="">
                                <div class="card__title">Лабытнанги</div>
                                <div class="card__text">посмотреть</div>
                            </div>
                        </a>
                        <a href="./event_page.html">
                            <div class="action__items-card card col-3"><img src="<?php echo bloginfo("template_url"); ?>/assets/img/Muravlenko.png" alt="">
                                <div class="card__title">Муравленко</div>
                                <div class="card__text">посмотреть</div>
                            </div>
                        </a>
                        <a href="./event_page.html">
                            <div class="action__items-card card col-3"><img src="<?php echo bloginfo("template_url"); ?>/assets/img/Nadim.png" alt="">
                                <div class="card__title">Надым</div>
                                <div class="card__text">посмотреть</div>
                            </div>
                        </a>
                        <a href="./event_page.html">
                            <div class="action__items-card card col-3"><img src="<?php echo bloginfo("template_url"); ?>/assets/img/Noyabrsk.png" alt="">
                                <div class="card__title">Ноябрьск</div>
                                <div class="card__text">посмотреть</div>
                            </div>
                        </a>
                        <a href="./event_page.html">
                            <div class="action__items-card card col-3"><img src="<?php echo bloginfo("template_url"); ?>/assets/img/Salehard.png" alt="">
                                <div class="card__title">Салехард</div>
                                <div class="card__text">посмотреть</div>
                            </div>
                        </a>
                        <a href="./event_page.html">
                            <div class="action__items-card card col-3"><img src="<?php echo bloginfo("template_url"); ?>/assets/img/Tazovski.png" alt="">
                                <div class="card__title">Тазовский</div>
                                <div class="card__text">посмотреть</div>
                            </div>
                        </a>
                        <a href="./event_page.html">
                            <div class="action__items-card card col-3"><img src="<?php echo bloginfo("template_url"); ?>/assets/img/TarkoSale.png" alt="">
                                <div class="card__title">Тарко-Сале</div>
                                <div class="card__text">посмотреть</div>
                            </div>
                        </a>
                        <a href="./event_page.html">
                            <div class="action__items-card card col-3"><img src="<?php echo bloginfo("template_url"); ?>/assets/img/Hanmei.png" alt="">
                                <div class="card__title">Ханымей</div>
                                <div class="card__text">посмотреть</div>
                            </div>
                        </a>
                    </div>
                </div>
            </div>
        </main>

        <?php get_footer() ?>
    </div>

    <?php wp_footer() ?>
</body>

</html>
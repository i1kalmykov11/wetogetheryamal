let selectListener = document.getElementById("event");
let biographyDisplay = document.getElementById("biographyDisplay");
let combatPathDisplay = document.getElementById("combatPathDisplay");
let formDisplay = document.getElementById("formDisplay");

selectListener.addEventListener("change", function() {
    formDisplay.classList.add("display-switcher");
    combatPathDisplay.classList.add("display-switcher");
    biographyDisplay.classList.add("display-switcher");

    if(selectListener.value == "Книга памяти Ямал")
    {
        formDisplay.classList.remove("display-switcher");
        combatPathDisplay.classList.remove("display-switcher");
        biographyDisplay.classList.remove("display-switcher");
    }
    if(selectListener.value == "История проходит через дом")
    {
        formDisplay.classList.remove("display-switcher");
        biographyDisplay.classList.remove("display-switcher");
    }
    if(selectListener.value == "Шарф Победы" || selectListener.value == "Письма войны" 
    || selectListener.value == "Мы помним")
    {
        formDisplay.classList.remove("display-switcher");
    }
});

function addActivityItem() {
    // ... Code to add item here
}

$('#images').change(function() {
    if (this.files.length > 0)
    {
        $('#labelForImages').text('');
        for (var i = 0; i < this.files.length; i++) {
            
            $('#labelForImages').append(this.files[i].name + '; ');
        }
    } 
    else {
        $('#labelForImages').text("Нажмите, чтобы выбрать файлы (Поддерживаемые форматы: JPG, PNG)");
    }
});
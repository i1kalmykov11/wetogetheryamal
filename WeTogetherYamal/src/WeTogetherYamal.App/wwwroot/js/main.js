
    let selectListener = document.getElementById("EventTypeId");
    let biographyDisplay = document.getElementById("biographyDisplay");
    let combatPathDisplay = document.getElementById("combatPathDisplay");
    let formDisplay = document.getElementById("formDisplay");

    formDisplay.classList.add("display-switcher");
    combatPathDisplay.classList.add("display-switcher");
    biographyDisplay.classList.add("display-switcher");
if (selectListener.value == "MemoryBookYamal") {
        formDisplay.classList.remove("display-switcher");
        combatPathDisplay.classList.remove("display-switcher");
        biographyDisplay.classList.remove("display-switcher");
    }
if (selectListener.value == "StoryRunsThroughHouse") {
        formDisplay.classList.remove("display-switcher");
        biographyDisplay.classList.remove("display-switcher");
    }
if (selectListener.value == "VictoryScarf" || selectListener.value == "WarLetters"
    || selectListener.value == "WeRemember") {
        formDisplay.classList.remove("display-switcher");
    }

    selectListener.addEventListener("change", function () {
        formDisplay.classList.add("display-switcher");
        combatPathDisplay.classList.add("display-switcher");
        biographyDisplay.classList.add("display-switcher");

        if (selectListener.value == "MemoryBookYamal") {
            formDisplay.classList.remove("display-switcher");
            combatPathDisplay.classList.remove("display-switcher");
            biographyDisplay.classList.remove("display-switcher");
        }
        if (selectListener.value == "StoryRunsThroughHouse") {
            formDisplay.classList.remove("display-switcher");
            biographyDisplay.classList.remove("display-switcher");
        }
        if (selectListener.value == "VictoryScarf" || selectListener.value == "WarLetters"
            || selectListener.value == "WeRemember") {
            formDisplay.classList.remove("display-switcher");
        }
    });

﻿using Lamar;
using Serilog;
using Serilog.Extensions.Hosting;

namespace WeTogetherYamal.Api;

public class SerilogConfiguration : ServiceRegistry
{
    public SerilogConfiguration(IConfiguration config)
    {
        Log.Logger = new LoggerConfiguration()
            .ReadFrom.Configuration(config)
            .CreateLogger();
        this.AddLogging(loggingBuilder =>
            loggingBuilder.AddSerilog());
        var implementationInstance = new DiagnosticContext(null);
        this.AddSingleton(implementationInstance);
        this.AddSingleton((IDiagnosticContext) implementationInstance);
    }
}
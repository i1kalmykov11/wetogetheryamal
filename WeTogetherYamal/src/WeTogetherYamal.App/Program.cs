using Lamar.Microsoft.DependencyInjection;
using WeTogetherYamal.Api.DataSeeder;

namespace WeTogetherYamal.Api;

public class Program
{
    public static void Main(string[] args)
    {
        CreateHostBuilder(args).Build()
            .SeedDatabases()
            .Run();
    }

    public static IHostBuilder CreateHostBuilder(string[] args)
    {
        return Host.CreateDefaultBuilder(args)
            .ConfigureLogging((context, logging) =>
            {
                logging.ClearProviders();
                logging.AddFilter("Microsoft.AspNetCore.Http.Connections", LogLevel.Debug);
            })
            .UseLamar((builder, registry) =>
            {
                registry.IncludeRegistry(new SerilogConfiguration(builder.Configuration));
            })
            .ConfigureWebHostDefaults(webBuilder => { webBuilder.UseStartup<Startup>(); });
    }
}
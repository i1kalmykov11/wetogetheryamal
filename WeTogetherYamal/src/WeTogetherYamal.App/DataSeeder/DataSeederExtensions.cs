﻿using WeTogetherYamal.Api.Identity;
using WeTogetherYamal.Infrastructure.Data;
using WeTogetherYamal.Infrastructure.Data.Extensions;

namespace WeTogetherYamal.Api.DataSeeder
{
    public static class DataSeederExtensions
    {
        public static IHost SeedDatabases(this IHost host)
        {
            using (var scope = host.Services.CreateScope())
            {
                var configuration = scope.ServiceProvider.GetService<IConfiguration>();
                var applySeed = (bool)configuration.GetValue(typeof(bool), "SeedDataOnApplicationStart", false);
                var logger = scope.ServiceProvider.GetService<ILogger<DatabaseConfigurationDataSeeder>>();
                if (!applySeed)
                {
                    logger.LogInformation("Skip data seed, SeedDataOnApplicationStart set to false or not present in the appSettings.json");
                    return host;
                }

                var context = scope.ServiceProvider.GetService<BusinessConfigurationDbContext>();
                if (context == null)
                {
                    throw new ArgumentException($"{nameof(BusinessConfigurationDbContext)} is not defined");
                }

                context.MigrateDatabase();

                var userManager = scope.ServiceProvider.GetService<ApplicationUserManager>();
                var dataSeeder = new DatabaseConfigurationDataSeeder(context, logger, userManager);
                dataSeeder.Seed().GetAwaiter().GetResult();
            }

            return host;
        }
    }
}
﻿using System.Net.Mail;
using Microsoft.EntityFrameworkCore;
using WeTogetherYamal.Api.Identity;
using WeTogetherYamal.Constants;
using WeTogetherYamal.Infrastructure.Data;
using WeTogetherYamal.SharedKernel.Entities;
using WeTogetherYamal.SharedKernel.Interfaces;

namespace WeTogetherYamal.Api.DataSeeder;

public class DatabaseConfigurationDataSeeder
{
    private readonly BusinessConfigurationDbContext _dbContext;
    private readonly ILogger<DatabaseConfigurationDataSeeder> _logger;
    private readonly ApplicationUserManager _userManager;

    private const string password = "Abc12345";

    public DatabaseConfigurationDataSeeder(BusinessConfigurationDbContext dbContext,
        ILogger<DatabaseConfigurationDataSeeder> logger, ApplicationUserManager userManager)
    {
        _dbContext = dbContext;
        _logger = logger;
        _userManager = userManager;
    }

    public async Task Seed()
    {
        _logger.LogInformation("Start seeding db");
        await SaveContextIfChanged();
        await SeedUserRolesAsync();
        await SeedTestEmployeeAsync();

        await SeedEnumType(default(CityEnum), _dbContext.CityTypes);
        await SeedEnumType(default(EventTypeEnum), _dbContext.EventTypes);

        await SaveContextIfChanged();

        _logger.LogInformation("Finish seeding db");
    }

    private async Task SeedTestEmployeeAsync()
    {
        _logger.LogInformation("Seeding Admin");
        if (!await _dbContext.Users.AnyAsync(x => x.Email == "admin@test.com"))
        {
            var emailAddress = new MailAddress("admin@test.com");
            var admin = new ApplicationUser()
            {
                CreatedAt = DateTimeOffset.UtcNow,
                Email = "admin@test.com",
                UserName = emailAddress.User
            };

            await _userManager.CreateAdminAsync(admin, password);
        }
    }

    private async Task SeedUserRolesAsync()
    {
        if (await _dbContext.Roles.AnyAsync())
            return;

        _logger.LogInformation("Seeding Roles");
        var roles = new List<ApplicationRole>
        {
            new ApplicationRole
            {
                Id = UserRoleIds.Admin,
                Name = UserRolesEnum.Admin.ToString(),
                NormalizedName = UserRolesEnum.Admin.ToString().ToUpperInvariant()
            }
        };
        await _dbContext.Roles.AddRangeAsync(roles);
        await _dbContext.SaveChangesAsync();
    }

    private async Task SeedEnumType<TTypeEnum, TEntityType>(TTypeEnum enumToAdd, DbSet<TEntityType> dbSet)
        where TTypeEnum : Enum
        where TEntityType : class, IEntityEnumType<TTypeEnum>, new()
    {
        var targetTypeName = typeof(TEntityType).Name;
        _logger.LogInformation("Looking \"{TargetTypeName}\" for seed new values..", targetTypeName);
        var values = Enum.GetValues(typeof(TTypeEnum)).Cast<TTypeEnum>().ToList();
        var typesToAdd = new List<TEntityType>();

        foreach (var optionType in values)
        {
            var entity = dbSet.FirstOrDefault(x => x.Id.Equals(optionType));
            if (entity is null)
            {
                _logger.LogInformation("Will add new \"{TargetTypeName}\" with value: \"{OptionType}\"",
                    targetTypeName, optionType);
                typesToAdd.Add(new TEntityType
                {
                    Id = optionType,
                    Name = optionType.ToString()
                });
            }
            else if (entity.Id.Equals(optionType) && !entity.Name.Equals(optionType.ToString()))
            {
                entity.Name = optionType.ToString();
            }
        }

        if (typesToAdd.Any())
        {
            await dbSet.AddRangeAsync(typesToAdd);
            _logger.LogInformation("Db table \"{TargetTypeName}s\" - successfully updated ", targetTypeName);
        }

        if (_dbContext.ChangeTracker.HasChanges())
        {
            await _dbContext.SaveChangesAsync();
            _logger.LogInformation("Db table \"{TargetTypeName}s\" - successfully updated ", targetTypeName);
        }
        else
        {
            _logger.LogInformation("Db table \"{TargetTypeName}s\" - nothing new to add", targetTypeName);
        }
    }

    private async Task SaveContextIfChanged()
    {
        if (_dbContext.ChangeTracker.HasChanges()) await _dbContext.SaveChangesAsync();
    }
}
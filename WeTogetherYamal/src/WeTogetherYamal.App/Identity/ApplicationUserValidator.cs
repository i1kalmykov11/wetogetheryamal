﻿using Microsoft.AspNetCore.Identity;
using WeTogetherYamal.Constants;
using WeTogetherYamal.Infrastructure.ApiValidators;
using WeTogetherYamal.Infrastructure.Data.Extensions;
using WeTogetherYamal.SharedKernel.Entities;

namespace WeTogetherYamal.Api.Identity
{
    public class ApplicationUserValidator : IUserValidator<ApplicationUser>
    {
        public IdentityErrorDescriber Describer { get; private set; }

        public ApplicationUserValidator(IdentityErrorDescriber errors = null)
        {
            Describer = (errors ?? new IdentityErrorDescriber());
        }

        public async Task<IdentityResult> ValidateAsync(UserManager<ApplicationUser> manager, ApplicationUser user)
        {
            var appUserManager = manager as ApplicationUserManager;
            if (appUserManager == null)
            {
                throw new InvalidCastException(
                    $"Application user manager should be of type {nameof(ApplicationUserManager)}");
            }

            if (user == null)
            {
                throw new ArgumentNullException(nameof(user));
            }
            List<IdentityError> errors = new List<IdentityError>();
            await ValidateEmail(appUserManager, user, errors);

            return (errors.Count > 0) 
                ? IdentityResult.Failed(errors.ToArray()) 
                : IdentityResult.Success;
        }

        private async Task ValidateEmail(ApplicationUserManager manager, ApplicationUser user, List<IdentityError> errors)
        {
            string email = await manager.GetEmailAsync(user);
            if (string.IsNullOrWhiteSpace(email))
            {
                errors.Add(Describer.InvalidEmail(email));
                return;
            }

            if (!new EmailValidatorAttribute().IsValid(email))
            {
                errors.Add(Describer.InvalidEmail(email));
                return;
            }

            ApplicationUser existingUser = null;
            if (user.GetRole() == UserRolesEnum.Admin)
            {
                existingUser = await manager.FindAdminByEmailAsync(email);
            }
            
            if (existingUser != null && user.Id != existingUser.Id)
            {
                errors.Add(Describer.DuplicateEmail(email));
            }
        }
    }
}
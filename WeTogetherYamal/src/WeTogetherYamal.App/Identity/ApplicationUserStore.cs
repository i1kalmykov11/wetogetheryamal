﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Identity.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore;
using WeTogetherYamal.Constants;
using WeTogetherYamal.Infrastructure.Data;
using WeTogetherYamal.SharedKernel.Entities;

namespace WeTogetherYamal.Api.Identity
{
    public class ApplicationUserStore :
        UserStore<ApplicationUser, ApplicationRole, BusinessConfigurationDbContext, Guid, IdentityUserClaim<Guid>,
            ApplicationUserRole, IdentityUserLogin<Guid>, IdentityUserToken<Guid>, IdentityRoleClaim<Guid>>
    {
        public ApplicationUserStore(
            BusinessConfigurationDbContext context,
            IdentityErrorDescriber describer = null)
            : base(context, describer)
        {
        }

        public async Task<bool> IsEmailExists(
            string normalizedEmail,
            CancellationToken cancellationToken = new CancellationToken())
        {
            return await UserQuery()
                .AnyAsync(x => x.NormalizedEmail == normalizedEmail);
        }

        public async Task<ApplicationUser> FindAdminByEmailAsync(string normalizedEmail)
        {
            return await UserQuery()
                .FirstOrDefaultAsync(x => x.NormalizedEmail == normalizedEmail &&
                                          x.UserRoles.Any(r => r.Role.Id == UserRoleIds.Admin));
        }

        public override async Task<IList<string>> GetRolesAsync(ApplicationUser user,
            CancellationToken cancellationToken = new CancellationToken())
        {
            return await Context.UserRoles
                .Where(x => x.UserId.Equals(user.Id))
                .Select(x => x.Role.Name)
                .ToListAsync();
        }

        public override async Task<ApplicationUser> FindByIdAsync(
            string userId, CancellationToken cancellationToken = new CancellationToken())
        {
            var guidId = Guid.Parse(userId);

            return await UserQuery()
                .FirstOrDefaultAsync(x => x.Id == guidId, cancellationToken);
        }

        public async Task<ApplicationUserRole> CreateUserRoleEntity(
            ApplicationUser user, string normalizedRole, CancellationToken cancellationToken = new CancellationToken())
        {
            var role = await FindRoleAsync(normalizedRole, cancellationToken);
            if (role == null)
            {
                return null;
            }

            return CreateUserRole(user, role);
        }

        private IQueryable<ApplicationUser> UserQuery()
        {
            return Context.Set<ApplicationUser>()
                .Include(x => x.UserRoles)
                .ThenInclude(x => x.Role);
        }
    }
}
﻿using System.Net.Mail;
using Microsoft.AspNetCore.Identity;
using Microsoft.Extensions.Options;
using WeTogetherYamal.Constants;
using WeTogetherYamal.Infrastructure.Data.Extensions;
using WeTogetherYamal.SharedKernel.Entities;

namespace WeTogetherYamal.Api.Identity
{
    public class ApplicationUserManager : UserManager<ApplicationUser>
    {
        private ApplicationUserStore _applicationUserStore => Store as ApplicationUserStore;

        public ApplicationUserManager(ApplicationUserStore store, IOptions<IdentityOptions> optionsAccessor,
            IPasswordHasher<ApplicationUser> passwordHasher,
            IEnumerable<IUserValidator<ApplicationUser>> userValidators,
            IEnumerable<IPasswordValidator<ApplicationUser>> passwordValidators, ILookupNormalizer keyNormalizer,
            IdentityErrorDescriber errors, IServiceProvider services, ILogger<ApplicationUserManager> logger)
            : base(store, optionsAccessor, passwordHasher, userValidators, passwordValidators, keyNormalizer, errors,
                services, logger)
        {
        }

        public async Task<ApplicationUser> FindAdminByEmailAsync(string email)
        {
            return await _applicationUserStore.FindAdminByEmailAsync(NormalizeEmail(email));
        }

        public async Task<bool> IsEmailExistsAsync(string email)
        {
            return await _applicationUserStore.IsEmailExists(NormalizeEmail(email));
        }

        public async Task AssignRole(ApplicationUser user, UserRolesEnum role)
        {
            var userRole = await _applicationUserStore
                .CreateUserRoleEntity(user, NormalizeName(role.ToString()), CancellationToken);

            user.UserRoles = new[] { userRole };
        }

        public async Task<IdentityResult> CreateAdminAsync(ApplicationUser user, string password)
        {
            if (user.Email == null)
            {
                return IdentityResult.Failed();
            }

            var emailAdress = new MailAddress(user.Email);
            user.UserName = emailAdress.User;

            await AssignRole(user, UserRolesEnum.Admin);
            var createUserResult = await CreateAsync(user, password);
            return createUserResult;
        }

        private IdentityResult InvalidRegistrationRequest()
        {
            return IdentityResultExtensions.InvalidRegistrationRequest();
        }
    }
}
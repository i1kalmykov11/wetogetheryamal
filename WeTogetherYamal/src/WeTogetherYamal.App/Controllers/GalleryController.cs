﻿using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;

namespace WeTogetherYamal.Api.Controllers
{
    [Route("/[controller]")]
    public class GalleryController : Controller
    {
        [AllowAnonymous]
        [HttpGet("memoryBookYamal")]
        public IActionResult Index()
        {
            return View();
        }
    }
}

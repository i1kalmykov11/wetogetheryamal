﻿using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using WeTogetherYamal.SharedKernel.Models.EventRequest;

namespace WeTogetherYamal.Api.Controllers
{
    [Route("/[controller]")]
    public class EventsController : Controller
    {
        [AllowAnonymous]
        [HttpGet("memoryBookYamal")]
        public IActionResult MemoryBookYamal()
        {
            return GetPageWithSubmitForm();
        }

        [AllowAnonymous]
        [HttpGet("storyRunsThroughHouse")]
        public IActionResult StoryRunsThroughHouse()
        {
            return GetPageWithSubmitForm();
        }
        
        [AllowAnonymous]
        [HttpGet("victoryScarf")]
        public IActionResult VictoryScarf()
        {
            return GetPageWithSubmitForm();
        }

        [AllowAnonymous]
        [HttpGet("warLetters")]
        public IActionResult WarLetters()
        {
            return GetPageWithSubmitForm();
        }

        [AllowAnonymous]
        [HttpGet("weRemember")]
        public IActionResult WeRemember()
        {
            return GetPageWithSubmitForm();
        }

        private IActionResult GetPageWithSubmitForm()
        {
            if (User.Identity.IsAuthenticated)
            {
                return RedirectToAction("GetEventRequestList", "AdminPanel");
            }
            var model = new CreateEventRequestViewModel();
            return View(model);
        }
    }
}

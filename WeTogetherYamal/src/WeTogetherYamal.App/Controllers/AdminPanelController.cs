﻿using Microsoft.AspNetCore.Authentication;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using WeTogetherYamal.Api.Identity;
using WeTogetherYamal.Api.ViewModels;
using WeTogetherYamal.Core.Interfaces;
using WeTogetherYamal.SharedKernel.Entities;
using WeTogetherYamal.SharedKernel.Interfaces;
using WeTogetherYamal.SharedKernel.Models.EventRequest;
using WeTogetherYamal.SharedKernel.Models.Search;
using SignInResult = Microsoft.AspNetCore.Identity.SignInResult;

namespace WeTogetherYamal.Api.Controllers
{
    [Route("/admin")]
    public class AdminPanelController : Controller
    {
        private readonly SignInManager<ApplicationUser> _signInManager;
        private readonly ApplicationUserManager _userManager;
        private readonly ILogger<AdminPanelController> _logger;
        private readonly IUserSecurityContext _userSecurityContext;
        private readonly ICoreEventRequestService _coreEventRequestService;

        public AdminPanelController(SignInManager<ApplicationUser> signInManager,
            ApplicationUserManager userManager,
            ILogger<AdminPanelController> logger,
            IUserSecurityContext userSecurityContext,
            ICoreEventRequestService coreEventRequestService
        )
        {
            _signInManager = signInManager ?? throw new ArgumentNullException(nameof(signInManager));
            _userManager = userManager ?? throw new ArgumentNullException(nameof(userManager));
            _logger = logger ?? throw new ArgumentNullException(nameof(logger));
            _userSecurityContext = userSecurityContext ?? throw new ArgumentNullException(nameof(userSecurityContext));
            _coreEventRequestService = coreEventRequestService ?? throw new ArgumentNullException(nameof(coreEventRequestService));
        }

        [Authorize(Roles = "Admin")]
        [HttpGet("eventRequestList")]
        public async Task<IActionResult> GetEventRequestList()
        {
            GetEventRequestListViewModel model = new GetEventRequestListViewModel();
            model.SearchModel = new EventRequestSearchModel();
            model.EventRecords = new EventRecord[] { };
            var result = await _coreEventRequestService.GetEventRequestListAsync(model.PagingModel, model.SearchModel);
            model.EventRecords = result;
            return View(model);
        }

        [Authorize(Roles = "Admin")]
        [HttpPost("eventRequestList")]
        public async Task<IActionResult> GetEventRequestList(GetEventRequestListViewModel model)
        {
            if (model == null)
            {
                return View(model);
            }

            var searchModel = new EventRequestSearchModel()
            {
                SearchProperties = new string[] { "FIO" },
                SearchValue = model.SearchModel?.SearchValue,
                City = model.SearchModel.City,
                EventType = model.SearchModel.EventType
            };
            model.SearchModel = searchModel;
            var result = await _coreEventRequestService.GetEventRequestListAsync(model.PagingModel, model.SearchModel);
            model.EventRecords = result;
            return View(model);
        }

        [AllowAnonymous]
        [HttpGet]
        public IActionResult Login(string returnUrl)
        {
            if (User.Identity.IsAuthenticated)
            {
                return RedirectToAction("GetEventRequestList");
            }
            ViewBag.IsModalLogin = true;
            var model = new UserLoginModel()
            {
                ReturnUrl = returnUrl
            };
            return View(model);
        }

        [AllowAnonymous]
        [HttpPost]
        public async Task<IActionResult> Login(UserLoginModel model)
        {
            ViewBag.IsModalLogin = true;
            if (User?.Identity.IsAuthenticated == true)
            {
                await _signInManager.SignOutAsync();
            }

            if (!ModelState.IsValid || model == null)
            {
                return View(model);
            }

            var user = await _userManager.FindByEmailAsync(model.Email);

            if (user != null && (await _signInManager.PasswordSignInAsync(user, model.Password, false, false) ==
                                 SignInResult.Success))
            {
                AuthenticationProperties props = null;
                if (model.RememberLogin)
                {
                    props = new AuthenticationProperties
                    {
                        IsPersistent = true,
                        ExpiresUtc = DateTimeOffset.UtcNow.Add(TimeSpan.FromDays(30))
                    };
                }

                await _signInManager.SignInAsync(user, props);
            }
            else
            {
                ModelState.AddModelError("Error", "Неверный логин или пароль");
                return View(model);
            }

            return RedirectToAction("Login");
        }

        [HttpGet("logout")]
        public async Task<IActionResult> Logout()
        {
            if (User?.Identity.IsAuthenticated == true)
            {
                await _signInManager.SignOutAsync();

                return Redirect("/");
            }

            return Redirect("/");
        }
    }
}

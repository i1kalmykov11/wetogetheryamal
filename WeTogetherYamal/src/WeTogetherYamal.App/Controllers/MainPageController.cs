﻿using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using WeTogetherYamal.Core.Interfaces;
using WeTogetherYamal.SharedKernel.Models.EventRequest;

namespace WeTogetherYamal.Api.Controllers
{
    [Route("/")]
    public class MainPageController : Controller
    {
        private readonly ICoreEventRequestService _eventRequestService;

        public MainPageController(ICoreEventRequestService eventRequestService)
        {
            _eventRequestService = eventRequestService ?? throw new ArgumentNullException(nameof(eventRequestService));
        }

        // GET: MainPageController
        [AllowAnonymous]
        [HttpGet]
        public IActionResult Index()
        {
            if (User.Identity.IsAuthenticated)
            {
                return RedirectToAction("GetEventRequestList", "AdminPanel");
            }
            var model = new CreateEventRequestViewModel();
            return View(model);
        }

        [HttpPost]
        public async Task<IActionResult> Index(CreateEventRequestViewModel model)
        {
            if (model == null || !ModelState.IsValid)
            {
                ViewData["ModelIsValid"] = false;
                return View(model);
            }

            await _eventRequestService.CreateEventRequestAsync(model);
            var returnUrl = model.ReturnUrl ?? "/";
            return Redirect(returnUrl);
        }
    }
}

﻿using FluentValidation;
using WeTogetherYamal.Constants;
using WeTogetherYamal.SharedKernel.Models.EventRequest;

namespace WeTogetherYamal.Api.Validators;

public class CreateEventRequestValidator : AbstractValidator<CreateEventRequestViewModel>
{
    private string _message = string.Empty;
    public CreateEventRequestValidator()
    {
        RuleLevelCascadeMode = CascadeMode.Stop;
        
        RuleFor(x => x.IsAcceptedConditions).Must(x => x == true)
            .WithMessage("Пожалуйста, дайте согласие на обработку ваших данных");

        RuleFor(x => x.EventTypeId).IsInEnum()
            .WithMessage("Необходимо заполнить поле");

        RuleFor(x => x.Address).NotEmpty()
            .WithMessage("Необходимо заполнить поле");

        RuleFor(x => x.FIO).NotEmpty()
            .WithMessage("Необходимо заполнить поле");

        RuleFor(x => x.Files).NotEmpty().WithMessage("Необходимо заполнить поле").DependentRules(() =>
        {
            RuleForEach(x => x.Files).Must(CheckFile).WithMessage(x => _message);
        });

        RuleFor(x => x.BirthDate).NotEmpty()
            .WithMessage("Необходимо заполнить поле")
            .Must(x => DateTimeOffset.UtcNow > x)
            .WithMessage("Дата рождения некорректна");

        RuleFor(x => x.PhoneNumber).NotEmpty()
            .WithMessage("Необходимо заполнить поле");

        RuleFor(x => x.CityId).IsInEnum()
            .WithMessage("Необходимо заполнить поле");

        RuleFor(x => x.Email)
            .NotEmpty().WithMessage("Необходимо заполнить поле")
            .EmailAddress()
            .WithMessage("Введите корректный e-mail");

        When(x => x.EventTypeId is EventTypeEnum.MemoryBookYamal, () =>
        {
            RuleFor(x => x.BiographyData).NotEmpty()
                .WithMessage("Необходимо заполнить поле");

            RuleFor(x => x.CombatVeteranPath).NotEmpty()
                .WithMessage("Необходимо заполнить поле");
        });

        When(x => x.EventTypeId is EventTypeEnum.StoryRunsThroughHouse, () =>
        {
            RuleFor(x => x.CombatVeteranPath).NotEmpty()
                .WithMessage("Необходимо заполнить поле");
        });
    }

    private bool CheckFile(IFormFile formFile)
    {
        if (!(string.Equals(formFile.ContentType, "image/jpeg", StringComparison.OrdinalIgnoreCase) ||
            string.Equals(formFile.ContentType, "image/jpg", StringComparison.OrdinalIgnoreCase) ||
            string.Equals(formFile.ContentType, "image/png", StringComparison.OrdinalIgnoreCase)))
        {
            _message = "Изображение не распознано, допустимые типы: .jpg .png .jpeg";
            return false;
        }

        return true;

    }
}
﻿using FluentValidation;

namespace WeTogetherYamal.Api.Validators
{
    public class FileValidator : AbstractValidator<IFormFile>
    {
        public FileValidator()
        {
            RuleFor(x => x).NotNull();

            RuleFor(x => x.Length).NotNull().LessThanOrEqualTo(3000000)
                .WithMessage("Размер файла должен быть меньше 3 Мб");

            RuleFor(x => x.ContentType).NotNull().Must(x =>
                    string.Equals(x, "image/jpeg", StringComparison.OrdinalIgnoreCase) ||
                    string.Equals(x, "image/jpg", StringComparison.OrdinalIgnoreCase) ||
                    string.Equals(x, "image/png", StringComparison.OrdinalIgnoreCase))
                .WithMessage("Изображение не распознано, допустимые типы: .jpg .png .jpeg");

            RuleFor(x => x.FileName).Must(fileName =>
            {
                var invalidChars = Path.GetInvalidFileNameChars();
                var containsInvalidCharacters = fileName.IndexOfAny(invalidChars) > -1;
                return Path.GetFileName(fileName).Length == fileName.Length
                       && !containsInvalidCharacters;
            }).WithMessage("Недопустимое имя файла");

        }
    }
}

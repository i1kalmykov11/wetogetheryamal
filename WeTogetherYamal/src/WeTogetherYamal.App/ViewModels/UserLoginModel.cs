﻿using System.ComponentModel.DataAnnotations;
using Microsoft.AspNetCore.Authentication;
using WeTogetherYamal.Infrastructure.ApiValidators;

namespace WeTogetherYamal.Api.ViewModels
{
    public class UserLoginModel
    {
        [Required]
        [EmailValidator]
        public string Email { get; set; }

        [Required]
        [DataType(DataType.Password)]
        public string Password { get; set; }

        public bool RememberLogin { get; set; } = false;

        public string ReturnUrl { get; set; }

        public IList<AuthenticationScheme> ExternalLogins { get; set; }
    }
}
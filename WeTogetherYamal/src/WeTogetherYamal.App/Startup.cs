﻿using System.Security.Claims;
using AutoMapper;
using AutoMapper.EquivalencyExpression;
using FluentValidation;
using Lamar;
using Microsoft.AspNetCore.Authentication.Cookies;
using Microsoft.AspNetCore.Identity;
using Serilog;
using Microsoft.AspNetCore.Authentication;
using WeTogetherYamal.Infrastructure.Data.Extensions;
using WeTogetherYamal.Constants;
using WeTogetherYamal.Infrastructure.Exceptions;
using WeTogetherYamal.SharedKernel.Entities;
using WeTogetherYamal.Infrastructure.Data;
using FluentValidation.AspNetCore;
using WeTogetherYamal.Api.Validators;
using WeTogetherYamal.Core.MapperProfiles;
using WeTogetherYamal.Infrastructure.Configuration;
using WeTogetherYamal.Infrastructure.Configuration.SettingsModels;
using WeTogetherYamal.SharedKernel.Models.EventRequest;

namespace WeTogetherYamal.Api;

public class Startup
{
    public Startup(IConfiguration configuration)
    {
        Configuration = configuration;
    }

    public IConfiguration Configuration { get; }

    public void ConfigureContainer(ServiceRegistry services)
    {
        services
            .AddHttpContextAccessor()
            .AddControllersWithViews()
            .AddFluentValidation();

        services.AddIdentity<ApplicationUser, ApplicationRole>(options =>
        {
            options.Password.RequiredLength = 8;
            options.Password.RequiredUniqueChars = 3;
            options.Password.RequireLowercase = false;
            options.Password.RequireUppercase = false;
            options.Password.RequireNonAlphanumeric = false;
            options.Password.RequireDigit = false;
            options.User.RequireUniqueEmail = true;
            options.SignIn.RequireConfirmedEmail = false;
        })
            .AddEntityFrameworkStores<BusinessConfigurationDbContext>()
            .AddDefaultTokenProviders();

        services.AddAuthentication(CookieAuthenticationDefaults.AuthenticationScheme)
            .AddCookie(options =>
            {
                options.LoginPath = new PathString("/Account/Login");
            });

        services.AddAuthorization(options =>
        {
            options.AddPolicy("User", policy =>
            {
                policy.RequireAuthenticatedUser();
                policy.RequireRole(new[]
                {
                        UserRolesEnum.Admin.ToString()
                    });
            });
        });

        services.AddUserSecurityContext(settings =>
        {
            settings.GetTokenAsyncFunction = context =>
                context.GetTokenAsync("access_token");
            settings.UserIdClaimType = ClaimTypes.NameIdentifier;
            settings.RoleClaimType = AuthorizationConstants.RoleClaimType;
        });

        services.AddAutoMapper(x =>
        {
            x.AddProfiles(new Profile[]
            {
                new MapperProfile()
            });
            x.AddCollectionMappers();
        }, AppDomain.CurrentDomain.GetAssemblies());

        services.For<FileSettings>().Add(new FileSettings(Configuration.GetSection("FileSettings"))).Singleton();
        services.IncludeRegistry(
             new BusinessConfigurationDbConfiguration(
                 Configuration.GetConnectionString("BusinessConfigurationSqlConnection")));

        services.IncludeRegistry(new CoreConfiguration());

        services
            .AddTransient<IValidator<CreateEventRequestViewModel>, CreateEventRequestValidator>();
        //     .AddTransient<IValidator<VeterinarianRegistrationModel>, VeterinarianRegistrationModelValidator>()
        //     .AddTransient<IValidator<AdminRegistrationModel>, AdminRegistrationModelValidator>()
        //     .AddTransient<IValidator<EditPatientProfileViewModel>, EditPatientProfileViewModelValidator>()
        //     .AddTransient<IValidator<CreateMedicalRecordViewModel>, CreateMedicalRecordViewModelValidator>()
        //     .AddTransient<IValidator<CreateMedicalServiceViewModel>, CreateMedicalServiceViewModelValidator>();


        services.AddRazorPages()
            .AddRazorRuntimeCompilation();
    }

    // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
    public void Configure(IApplicationBuilder app, IWebHostEnvironment env)
    {
        if (env.IsDevelopment())
        {
            app.UseDeveloperExceptionPage();
        }

        app.UseHttpsRedirection();
        app.UseMiddleware<GlobalExceptionHandler>();
        app.UseStaticFiles();

        app.UseRouting();

        app.UseSerilogRequestLogging();
        app.UseAuthentication();
        app.UseAuthorization();
        app.UseUserSecurityContext();

        app.UseEndpoints(endpoints =>
        {
            endpoints.MapControllers();
        });
    }
}
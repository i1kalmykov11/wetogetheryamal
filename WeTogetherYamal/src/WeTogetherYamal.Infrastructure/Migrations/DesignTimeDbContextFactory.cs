﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Design;
using Microsoft.Extensions.Configuration;
using WeTogetherYamal.Infrastructure.Data;

namespace WeTogetherYamal.Infrastructure.Migrations
{
    class DesignTimeDbContextFactory : IDesignTimeDbContextFactory<BusinessConfigurationDbContext>
    {
        public BusinessConfigurationDbContext CreateDbContext(string[] args)
        {
            var configuration = new ConfigurationBuilder()
                .AddJsonFile("infrastructureSettings.json")
                .Build();
            var connectionString = configuration.GetConnectionString("BusinessConfigurationSqlConnection");
            var dbOptionsBuilder = new DbContextOptionsBuilder<BusinessConfigurationDbContext>();
            dbOptionsBuilder.UseSqlServer(connectionString);

            return new BusinessConfigurationDbContext(dbOptionsBuilder.Options);
        }
    }
}

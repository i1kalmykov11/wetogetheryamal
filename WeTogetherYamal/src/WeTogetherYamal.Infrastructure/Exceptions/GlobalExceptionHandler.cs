﻿using System.Net;
using Microsoft.AspNetCore.Http;
using Microsoft.Extensions.Logging;
using Newtonsoft.Json;

namespace WeTogetherYamal.Infrastructure.Exceptions;

public class GlobalExceptionHandler
{
    private readonly RequestDelegate _next;
    private readonly ILogger<GlobalExceptionHandler> _logger;

    public GlobalExceptionHandler(RequestDelegate next, ILogger<GlobalExceptionHandler> logger)
    {
        _next = next;
        _logger = logger;
    }

    public async Task InvokeAsync(HttpContext httpContext)
    {
        try
        {
            await _next(httpContext);
        }
        catch (Exception e)
        {
            _logger.LogError(e,
                $"Error message: {e.Message}, Exception type: {e.GetType()} request: {SerializeRequest(httpContext.Request)}");
            await HandleGlobalExceptionAsync(httpContext, e);
        }
    }

    private static Task HandleGlobalExceptionAsync(HttpContext context, Exception exception)
    {
        var responseStatusCode = HttpStatusCode.InternalServerError;
        if (exception is ApplicationStatusCodeException codeException)
            responseStatusCode = codeException.ResponseStatusCode;
        context.Response.ContentType = "application/json";
        context.Response.StatusCode = (int) responseStatusCode;
        return Task.CompletedTask;
    }

    private string SerializeRequest(HttpRequest request)
    {
        var headers = JsonConvert.SerializeObject(request.Headers);
        var host = request.Host.Value;
        var path = request.Path.Value;
        var method = request.Method;
        var scheme = request.Scheme;
        return $"{method} {scheme}://{host}{path} Headers: {headers}";
    }
}
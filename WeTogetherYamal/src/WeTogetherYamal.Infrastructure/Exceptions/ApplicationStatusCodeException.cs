﻿using System.Net;

namespace WeTogetherYamal.Infrastructure.Exceptions;

public abstract class ApplicationStatusCodeException : Exception
{
    private readonly HttpStatusCode _responseStatusCode;

    public HttpStatusCode ResponseStatusCode => _responseStatusCode;

    public ApplicationStatusCodeException(HttpStatusCode responseStatusCode)
    {
        _responseStatusCode = responseStatusCode;
    }

    public ApplicationStatusCodeException(string message, HttpStatusCode responseStatusCode)
        : base(message)
    {
        _responseStatusCode = responseStatusCode;
    }

    public ApplicationStatusCodeException(string message)
        : base(message)
    {
    }
}
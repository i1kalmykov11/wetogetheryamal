﻿using System.Net;

namespace WeTogetherYamal.Infrastructure.Exceptions;

public class UnauthorizedRequestStatusCodeException : ApplicationStatusCodeException
{
    public UnauthorizedRequestStatusCodeException(string message)
        : base(message, HttpStatusCode.Unauthorized)
    {
    }
}
﻿using System.Net;

namespace WeTogetherYamal.Infrastructure.Exceptions;

public class ForbiddenRequestStatusCodeException : ApplicationStatusCodeException
{
    public ForbiddenRequestStatusCodeException(string message)
        : base(message, HttpStatusCode.Forbidden)
    {
    }
}
﻿using System.Security.Claims;
using Microsoft.AspNetCore.Http;
using WeTogetherYamal.Constants;
using WeTogetherYamal.Infrastructure.Configuration.SettingsModels;
using WeTogetherYamal.Infrastructure.Data.Extensions;
using WeTogetherYamal.SharedKernel.Interfaces;

namespace WeTogetherYamal.Infrastructure.Middleware
{
    public class UserSecurityContextMiddleware
    {
        private readonly RequestDelegate _next;
        private readonly SecurityContextSettings _settings;

        public UserSecurityContextMiddleware(RequestDelegate next, SecurityContextSettings contextSettings)
        {
            _next = next;
            _settings = contextSettings ?? throw new ArgumentNullException(nameof(contextSettings));
        }

        public async Task InvokeAsync(HttpContext context)
        {
            bool? userIsAuthenticated = context?.User?.Identity?.IsAuthenticated;

            if (userIsAuthenticated == null || userIsAuthenticated == false)
            {
                await _next(context);
                return;
            }

            var user = context.User;

            Guid userId = user.FindFirstValue(_settings.UserIdClaimType).GetGuid() ?? throw new Exception("No user id");
            var roleClaimValue = user.FindFirstValue(_settings.RoleClaimType);
            var email = user.FindFirstValue(ClaimTypes.Email);

            if (context.RequestServices.GetService(typeof(IUserSecurityContext)) is IUserSecurityContext securityContext)
            {
                securityContext.Role = Enum.Parse<UserRolesEnum>(roleClaimValue, true);
                securityContext.UserId = userId;
                securityContext.Email = email;
            }

            await _next(context);
        }
    }
}

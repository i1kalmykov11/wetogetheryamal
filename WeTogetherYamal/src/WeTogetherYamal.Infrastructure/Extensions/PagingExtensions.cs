﻿using System.Linq.Dynamic.Core;
using System.Linq.Expressions;
using System.Reflection;
using WeTogetherYamal.SharedKernel.Models.Paging;

namespace WeTogetherYamal.Infrastructure.Extensions
{
    public static class PagingExtensions
    {
        public static IQueryable<TEntity> ApplyPaging<TEntity>(this IQueryable<TEntity> query, PagingModel paging)
        where TEntity : class
        {
            if (paging == null) return query;

            var isAscOrder = true;
            if (!string.IsNullOrEmpty(paging.Order) && !string.IsNullOrWhiteSpace(paging.Order))
                isAscOrder = paging.Order.OrderTypeOrAsc();

            if (string.IsNullOrEmpty(paging.OrderBy))
            {
                var prop = typeof(TEntity).GetProperties().FirstOrDefault();
                query = query.OrderByPropertyName(prop.Name, isAscOrder);
            }
            else
            {
                query = query.OrderByPropertyName(paging.OrderBy, isAscOrder);
            }

            if (paging.Offset.HasValue && paging.Offset > 0) query = query.Skip(paging.Offset.Value);

            if (paging.Limit.HasValue && paging.Limit > 0) query = query.Take(paging.Limit.Value);

            return query;
        }

        public static Expression<Func<TEntity, TProperty>> Build<TEntity, TProperty>(PropertyInfo propertyInfo)
        {
            var parameterExpression = Expression.Parameter(typeof(TEntity));
            var field = Expression.PropertyOrField(parameterExpression, propertyInfo.Name);
            Expression conversion = Expression.Convert(field, typeof(object));
            return Expression.Lambda<Func<TEntity, TProperty>>(conversion, parameterExpression);
        }

        private static IQueryable<TEntity> OrderByPropertyName<TEntity>(this IQueryable<TEntity> query,
            string propertyName, bool isAscOrder = default)
        {
            if (string.IsNullOrEmpty(propertyName)) return query;

            var propertyInfo = typeof(TEntity).GetProperty(propertyName);
            if (propertyInfo == null) return query;

            return isAscOrder
                ? query.OrderBy($"{propertyName}")
                : query.OrderBy($"{propertyName} DESC");
        }

        private static bool OrderTypeOrAsc(this string property)
        {
            return property.ToLower().Contains("asc");
        }
    }
}
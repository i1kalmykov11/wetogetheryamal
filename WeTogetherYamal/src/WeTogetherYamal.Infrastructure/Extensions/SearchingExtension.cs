﻿using System.Linq.Expressions;
using WeTogetherYamal.SharedKernel.Models.Search;

namespace WeTogetherYamal.Infrastructure.Extensions
{
    public static class SearchingExtension
    {
        public static IQueryable<TEntity> ApplySearch<TEntity>(this IQueryable<TEntity> query, SearchModel model)
            where TEntity : class
        {
            if (model == null)
                return query;

            var searchProperties = model.SearchProperties;
            var searchValue = model.SearchValue;

            if (searchProperties == null || !searchProperties.Any())
                return query;

            if (string.IsNullOrEmpty(searchValue))
                return query;

            if (query == null || !query.Any())
                return query;

            var predicate = PredicateBuilder.False<TEntity>();
            var propsNotExist = searchProperties.Except(typeof(TEntity).GetProperties().Select(p => p.Name)).ToArray();
            if (propsNotExist.Any())
            {
                searchProperties = searchProperties.Except(propsNotExist).ToArray();
            }

            foreach (var propertyName in searchProperties)
            {
                var lambda = GetExpression<TEntity>(propertyName, searchValue);
                predicate = predicate.Or(lambda);
            }

            return query.Where(predicate);
        }

        private static Expression<Func<T, bool>> GetExpression<T>(string propertyName, string propertyValue)
        {
            var parameterExp = Expression.Parameter(typeof(T), "item");
            var propertyExp = Expression.Property(parameterExp, propertyName);
            var methodContains = typeof(string).GetMethod("Contains", new[] { typeof(string) });
            var someValue = Expression.Constant(propertyValue, typeof(string));
            var nullMethodExp = Expression.Call(typeof(string), nameof(string.IsNullOrEmpty), null, propertyExp);
            var containsMethodExp = Expression.Call(propertyExp, methodContains, someValue);
            var body = Expression.AndAlso(Expression.Not(nullMethodExp), containsMethodExp);
            return Expression.Lambda<Func<T, bool>>(body, parameterExp);
        }
    }
}

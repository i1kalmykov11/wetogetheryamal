﻿using Lamar;
using Microsoft.Extensions.DependencyInjection;

namespace WeTogetherYamal.Infrastructure.Configuration;

public class CoreConfiguration : ServiceRegistry
{
    public CoreConfiguration()
    {
        Scan(x =>
        {
            x.Assembly("WeTogetherYamal.Core");
            x.WithDefaultConventions(ServiceLifetime.Scoped);
        });
    }
}

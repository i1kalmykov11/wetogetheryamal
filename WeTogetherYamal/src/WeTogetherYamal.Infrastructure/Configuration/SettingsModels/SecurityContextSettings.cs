﻿using Microsoft.AspNetCore.Http;

namespace WeTogetherYamal.Infrastructure.Configuration.SettingsModels;

public class SecurityContextSettings
{
    public Func<HttpContext, Task<string>> GetTokenAsyncFunction { get; set; }

    public string UserIdClaimType { get; set; }

    public string RoleClaimType { get; set; }
}

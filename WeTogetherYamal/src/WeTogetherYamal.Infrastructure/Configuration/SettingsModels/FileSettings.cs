﻿using Microsoft.Extensions.Configuration;

namespace WeTogetherYamal.Infrastructure.Configuration.SettingsModels;

public class FileSettings
{
    public FileSettings(IConfigurationSection fileSection)
    {
        ImageStorage = fileSection.GetValue<string>("ImageStorage");
    }

    public string ImageStorage { get; }
}
﻿using Lamar;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.DependencyInjection;
using WeTogetherYamal.Infrastructure.Configuration.SettingsModels;
using WeTogetherYamal.Infrastructure.Data;
using WeTogetherYamal.Infrastructure.Data.Repository;
using WeTogetherYamal.SharedKernel.Interfaces.Repository;
using WeTogetherYamal.SharedKernel.Models;

namespace WeTogetherYamal.Infrastructure.Configuration;
public class BusinessConfigurationDbConfiguration : ServiceRegistry
{
    public BusinessConfigurationDbConfiguration(string connectionString)
    {
        Scan(x =>
        {
            x.Assembly("WeTogetherYamal.Infrastructure");
            x.WithDefaultConventions(ServiceLifetime.Scoped);
            x.ExcludeType<UnitOfWork>();
            x.ExcludeType<BusinessConfigurationDbContext>();
        });

        this.AddDbContext<BusinessConfigurationDbContext>(options =>
        {
            options.UseSqlServer(
                connectionString,
                builder =>
                {
                    builder.MigrationsAssembly("WeTogetherYamal.Infrastructure");
                });
        });

        For<IUnitOfWork>().Use<UnitOfWork>().Ctor<DbContext>("dbContext").Is(x => x.GetInstance<BusinessConfigurationDbContext>()).Scoped();
    }
}

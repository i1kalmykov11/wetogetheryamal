﻿using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.DependencyInjection.Extensions;
using WeTogetherYamal.Infrastructure.Configuration.SettingsModels;
using WeTogetherYamal.SharedKernel.Interfaces;
using WeTogetherYamal.SharedKernel.Models;

namespace WeTogetherYamal.Infrastructure.Configuration;

public static class UserSecurityContextServiceCollection
{
    public static IServiceCollection AddUserSecurityContext(this IServiceCollection services, Action<SecurityContextSettings> setupSettings)
    {
        if (setupSettings != null)
        {
            var settings = new SecurityContextSettings();
            setupSettings.Invoke(settings);
            services.TryAddSingleton<SecurityContextSettings>(settings);
        }
        else
        {
            throw new ArgumentNullException("setupSettings argument have to be provided");
        }

        services.AddScoped<IUserSecurityContext, UserSecurityContext>(sp => ActivatorUtilities.CreateInstance<UserSecurityContext>(sp));
        return services;
    }
}


﻿using System.ComponentModel.DataAnnotations;
using System.Net.Mail;

namespace WeTogetherYamal.Infrastructure.ApiValidators
{
    [AttributeUsage(AttributeTargets.Property | AttributeTargets.Field | AttributeTargets.Parameter,
        AllowMultiple = false)]
    public class EmailValidatorAttribute : DataTypeAttribute
    {
        public EmailValidatorAttribute()
            : base(DataType.EmailAddress)
        {
        }

        public EmailValidatorAttribute(string customDataType) : base(customDataType)
        {
        }

        public override bool IsValid(object value)
        {
            var email = value as string;
            if (email == null)
            {
                return false;
            }

            try
            {
                MailAddress address = new MailAddress(email);
                if (address.Host.Any(x => x.Equals('_')))
                {
                    return false;
                }

                if (address.User.Length > 64)
                {
                    return false;
                }
            }
            catch (Exception)
            {
                return false;
            }

            return true;
        }
    }
}
﻿using System;
using System.Linq;
using Microsoft.EntityFrameworkCore;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore.ChangeTracking;
using WeTogetherYamal.SharedKernel.Interfaces.Repository;

namespace WeTogetherYamal.Infrastructure.Data.Repository
{
    public class UnitOfWork : IUnitOfWork
    {
        private readonly DbContext _dbContext;
        public UnitOfWork(DbContext dbContext)
        {
            _dbContext = dbContext;
        }

        public int SaveChanges()
        {
            return _dbContext.SaveChanges();
        }

        public async Task<int> SaveChangesAsync()
        {
            return await _dbContext.SaveChangesAsync();
        }

        public async Task<ITransaction> BeginTransactionAsync()
        {
            var transaction = await _dbContext.Database.BeginTransactionAsync();
            return new SqlTransaction(transaction);
        }

        public bool HasChanges()
        {
            return _dbContext.ChangeTracker.HasChanges();
        }

        public TEntity[] GetModifiedEntities<TEntity>() where TEntity : class
        {
            return GetEntitiesInState<TEntity>(x => x.State == EntityState.Modified);
        }

        public TEntity[] GetChangedEntities<TEntity>() where TEntity : class
        {
            return GetEntitiesInState<TEntity>(x => x.State != EntityState.Unchanged);
        }

        public TEntity[] GetAddedEntities<TEntity>() where TEntity : class
        {
            return GetEntitiesInState<TEntity>(x => x.State == EntityState.Added);
        }

        private TEntity[] GetEntitiesInState<TEntity>(Func<EntityEntry<TEntity>, bool> predicate) where TEntity : class
        {
            return _dbContext.ChangeTracker
                .Entries<TEntity>()
                .Where(predicate)
                .Select(x => x.Entity)
                .ToArray();
        }
    }
}
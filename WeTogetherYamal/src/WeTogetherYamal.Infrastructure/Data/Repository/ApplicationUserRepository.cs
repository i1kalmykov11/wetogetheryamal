﻿using WeTogetherYamal.SharedKernel.Entities;
using WeTogetherYamal.SharedKernel.Interfaces.Repository;

namespace WeTogetherYamal.Infrastructure.Data.Repository
{
    public class ApplicationUserRepository : GenericRepository<ApplicationUser>, IApplicationUserRepository
    {
        public ApplicationUserRepository(BusinessConfigurationDbContext context)
            : base(context)
        {

        }
    }
}
﻿using System.Net.Mime;
using Microsoft.AspNetCore.Http;
using WeTogetherYamal.SharedKernel.Interfaces.Repository;
using File = WeTogetherYamal.SharedKernel.Entities.File;

namespace WeTogetherYamal.Infrastructure.Data.Repository;

public class FileRepository : GenericRepository<File>, IFileRepository
{
    public FileRepository(BusinessConfigurationDbContext context)
        : base(context)
    {
    }

}
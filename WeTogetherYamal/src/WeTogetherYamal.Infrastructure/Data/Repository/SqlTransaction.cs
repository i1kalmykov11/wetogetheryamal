﻿using WeTogetherYamal.SharedKernel.Interfaces.Repository;
using Microsoft.EntityFrameworkCore.Storage;

namespace WeTogetherYamal.Infrastructure.Data.Repository;

public class SqlTransaction : ITransaction
{
    private readonly IDbContextTransaction _relationalTransaction;
    private bool _disposed;

    public SqlTransaction(IDbContextTransaction relationalTransaction)
    {
        _relationalTransaction = relationalTransaction;
    }

    public void Dispose()
    {
        if (!_disposed)
        {
            _relationalTransaction.Dispose();
            _disposed = true;
        }
    }

    public async ValueTask DisposeAsync()
    {
        if (!_disposed)
        {
            _disposed = true;
            await _relationalTransaction.DisposeAsync();
        }
    }

    public async Task CommitAsync(CancellationToken cancellationToken = default)
    {
        await _relationalTransaction.CommitAsync(cancellationToken);
    }

    public async Task RollbackAsync(CancellationToken cancellationToken = default)
    {
        await _relationalTransaction.RollbackAsync(cancellationToken);
    }
}
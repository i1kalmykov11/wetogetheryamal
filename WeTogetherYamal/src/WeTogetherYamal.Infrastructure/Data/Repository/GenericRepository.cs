﻿using System.Linq.Expressions;
using Microsoft.EntityFrameworkCore;
using WeTogetherYamal.SharedKernel.Interfaces;
using WeTogetherYamal.SharedKernel.Interfaces.Repository;

namespace WeTogetherYamal.Infrastructure.Data.Repository
{
    public class GenericRepository<TEntity> : IRepository<TEntity> where TEntity : class
    {
        protected readonly DbContext Context;

        protected DbSet<TEntity> EntitySet { get; }

        public GenericRepository(DbContext context)
        {
            Context = context ?? throw new ArgumentNullException(nameof(context));
            EntitySet = Context.Set<TEntity>();
        }
        public void Add(TEntity entity)
        {
            EntitySet.Add(entity);
        }

        public void AddRange(IEnumerable<TEntity> entities)
        {
            EntitySet.AddRange(entities);
        }

        public IEnumerable<TEntity> Find(Expression<Func<TEntity, bool>> expression)
        {
            return EntitySet.Where(expression);
        }

        public TEntity GetById(object id)
        {
            return EntitySet.Find(id);
        }

        public async Task<TEntity> GetByIdAsync(object id)
        {
            return await EntitySet.FindAsync(id);
        }

        public bool DoesEntityExists<TType>(TType id) where TType : struct
        {
            CheckIIdentifierInherited<TType>();
            return EntitySet.Cast<IOrder.IIdentifier<TType>>()
                .AsNoTracking()
                .Count(x => x.Id.Equals(id)) > 0;
        }

        public bool DoesSetOfEntitiesExist<TType>(ICollection<TType> ids) where TType : struct
        {
            CheckIIdentifierInherited<TType>();
            return EntitySet.Cast<IOrder.IIdentifier<TType>>()
                .AsNoTracking()
                .Count(x => ids.Contains(x.Id)) == ids.Count;
        }

        public void Remove(TEntity entity)
        {
            EntitySet.Remove(entity);
        }

        public void RemoveRange(IEnumerable<TEntity> entities)
        {
            EntitySet.RemoveRange(entities);
        }

        public void AttachEntity(TEntity entity)
        {
           EntitySet.Attach(entity);
        }

        private void CheckIIdentifierInherited<TType>() where TType : struct
        {
            if (typeof(TEntity).GetInterface(typeof(IOrder.IIdentifier<TType>).Name) == null)
            {
                throw new Exception(
                    $"Entity of type:\"{typeof(TEntity).Name}\" have to derive from \"{typeof(IOrder.IIdentifier<TType>).Name}\".");
            }
        }
    }
}
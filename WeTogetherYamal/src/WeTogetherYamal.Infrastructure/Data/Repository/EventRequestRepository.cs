﻿using Microsoft.EntityFrameworkCore;
using WeTogetherYamal.Infrastructure.Extensions;
using WeTogetherYamal.SharedKernel.Entities;
using WeTogetherYamal.SharedKernel.Interfaces.Repository;
using WeTogetherYamal.SharedKernel.Models.EventRequest;
using WeTogetherYamal.SharedKernel.Models.Paging;
using WeTogetherYamal.SharedKernel.Models.Search;

namespace WeTogetherYamal.Infrastructure.Data.Repository;

public class EventRequestRepository : GenericRepository<EventRequest>, IEventRequestRepository
{
    public EventRequestRepository(BusinessConfigurationDbContext context)
        : base(context)
    {
    }

    public async Task<EventRecord[]> GetSearchedAndPaginatedEventRequestList(EventRequestSearchModel searchModel,
        PagingModel pagingModel)
    {
        var query = EntitySet.AsQueryable();

        if (searchModel != null && searchModel.EventType.HasValue)
        {
            query = query.Where(x => x.EventTypeId == searchModel.EventType);
        }

        if (searchModel != null && searchModel.City.HasValue)
        {
            query = query.Where(x => x.CityId == searchModel.City);
        }

        return await query
            .Include(x => x.Images)
            .Select(e => new EventRecord
            {
                Address = e.Address,
                Email = e.Email,
                BiographyData = e.BiographyData,
                BirthDate = e.BirthDate,
                CityEnum = e.CityId,
                CombatVeteranPath = e.CombatVeteranPath,
                EventEnum = e.EventTypeId,
                FIO = e.FIO,
                PhoneNumber = e.PhoneNumber,
                FilePathes = e.Images.Select(x => x.FilePath).ToArray()
            }).ApplySearch(searchModel)
            .ApplyPaging(pagingModel)
            .ToArrayAsync();
    }
}

﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using WeTogetherYamal.SharedKernel.Entities;

namespace WeTogetherYamal.Infrastructure.Data.Mappings;

public class CityTypeMap : IEntityTypeConfiguration<CityType>
{
    public void Configure(EntityTypeBuilder<CityType> builder)
    {
        builder.HasKey(x => x.Id);
        builder.Property(x => x.Id)
            .ValueGeneratedNever()
            .HasConversion<int>();

        builder.Property(x => x.Name)
            .IsRequired();

        builder.HasMany(x => x.EventRequests)
            .WithOne(x => x.CityType)
            .HasForeignKey(x => x.CityId)
            .IsRequired();
    }
}
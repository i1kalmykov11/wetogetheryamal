﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using WeTogetherYamal.SharedKernel.Entities;
using File = WeTogetherYamal.SharedKernel.Entities.File;

namespace WeTogetherYamal.Infrastructure.Data.Mappings;

public class EventTypeMap : IEntityTypeConfiguration<EventType>
{
    public void Configure(EntityTypeBuilder<EventType> builder)
    {
        builder.HasKey(x => x.Id);
        builder.Property(x => x.Id)
            .ValueGeneratedNever()
            .HasConversion<int>();

        builder.Property(x => x.Name)
            .IsRequired();

        builder.HasOne(x => x.Logo)
            .WithOne(x => x.EventType)
            .HasForeignKey<File>(x => x.EventTypeId)
            .IsRequired(false);

        builder.HasMany(x => x.EventRequests)
            .WithOne(x => x.EventType)
            .HasForeignKey(x => x.EventTypeId)
            .IsRequired();
    }
}
﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace WeTogetherYamal.Infrastructure.Data.Mappings;

public class FileMap : IEntityTypeConfiguration<SharedKernel.Entities.File>
{
    public void Configure(EntityTypeBuilder<SharedKernel.Entities.File> builder)
    {
        builder.HasKey(x => x.Id);
        builder.Property(x => x.Id).ValueGeneratedOnAdd();

        builder.Property(x => x.FileName)
            .IsRequired();

        builder.Property(x => x.FilePath)
            .IsRequired();

        builder.HasOne(a => a.CityType)
            .WithMany(x => x.Images)
            .HasForeignKey(c => c.CityTypeId)
            .IsRequired(false);

        builder.HasOne(a => a.ThumbFile)
            .WithOne(b => b.MainFile)
            .HasForeignKey<SharedKernel.Entities.File>(e => e.ThumbFileId)
            .IsRequired(false)
            .OnDelete(DeleteBehavior.NoAction);
    }
}
﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using WeTogetherYamal.SharedKernel.Entities;

namespace WeTogetherYamal.Infrastructure.Data.Mappings;

public class EventRequestMap : IEntityTypeConfiguration<EventRequest>
{
    public void Configure(EntityTypeBuilder<EventRequest> builder)
    {
        builder.HasKey(x => x.Id);
        builder.Property(x => x.Id).ValueGeneratedOnAdd();

        builder.Property(x => x.FIO)
            .IsRequired();

        builder.Property(x => x.BirthDate)
            .IsRequired();

        builder.Property(x => x.EventTypeId)
            .HasConversion<int>()
            .IsRequired();

        builder.Property(x => x.CityId)
            .HasConversion<int>()
            .IsRequired();

        builder.Property(x => x.PhoneNumber)
            .IsRequired();

        builder.Property(x => x.BiographyData)
            .HasColumnType("nvarchar(max)")
            .IsRequired(false);

        builder.Property(x => x.CombatVeteranPath)
            .HasColumnType("nvarchar(max)")
            .IsRequired(false);

        builder.Property(x => x.Email)
            .IsRequired();

        builder.Property(x => x.Address)
            .HasColumnType("nvarchar(max)")
            .IsRequired();

        builder.HasMany(x => x.Images)
            .WithOne(x => x.EventRequest)
            .HasForeignKey(x => x.EventRequestId)
            .OnDelete(DeleteBehavior.Cascade)
            .IsRequired(false);
    }
}
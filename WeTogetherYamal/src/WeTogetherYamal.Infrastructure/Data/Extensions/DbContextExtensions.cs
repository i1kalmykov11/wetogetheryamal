﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Infrastructure;
using Microsoft.EntityFrameworkCore.Migrations;

namespace WeTogetherYamal.Infrastructure.Data.Extensions
{
    public static class DbContextExtensions
    {
        public static void MigrateDatabase(this DbContext context)
        {
            var migrationAssembly = context.Database.GetService<IMigrationsAssembly>();
            var migrations = context.Database.GetMigrations();
            if (context.Database.GetPendingMigrations().Any())
            {
                context.Database.Migrate();
            }
        }
    }
}

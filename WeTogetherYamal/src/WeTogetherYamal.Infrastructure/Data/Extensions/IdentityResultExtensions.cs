﻿using Microsoft.AspNetCore.Identity;

namespace WeTogetherYamal.Infrastructure.Data.Extensions
{
    public static class IdentityResultExtensions
    {
        public static IdentityResult InvalidRegistrationRequest()
        {
            return IdentityResult.Failed(new IdentityError()
            {
                Code = "INVALID_REGISTRATION_REQUEST",
                Description = "Request is invalid"
            });
        }
    }
}
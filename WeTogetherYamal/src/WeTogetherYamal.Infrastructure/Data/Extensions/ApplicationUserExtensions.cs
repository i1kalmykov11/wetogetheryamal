﻿using WeTogetherYamal.Constants;
using WeTogetherYamal.SharedKernel.Entities;

namespace WeTogetherYamal.Infrastructure.Data.Extensions
{
    public static class ApplicationUserExtensions
    {
        public static UserRolesEnum GetRole(this ApplicationUser user)
        {
            if (user?.UserRoles?.FirstOrDefault() == null)
            {
                throw new ArgumentNullException(nameof(user.UserRoles), "user doesn't have UserRoles collection");
            }

            var role = user.UserRoles.First();
            return role.RoleId.GetUserRoleById();
        }
    }
}
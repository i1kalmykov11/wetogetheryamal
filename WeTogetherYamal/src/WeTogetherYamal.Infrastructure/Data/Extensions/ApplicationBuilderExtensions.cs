﻿using Microsoft.AspNetCore.Builder;
using WeTogetherYamal.Infrastructure.Middleware;

namespace WeTogetherYamal.Infrastructure.Data.Extensions
{
    public static class ApplicationBuilderExtensions
    {
        public static IApplicationBuilder UseUserSecurityContext(this IApplicationBuilder builder)
        {
            return builder.UseMiddleware<UserSecurityContextMiddleware>();
        }
    }
}

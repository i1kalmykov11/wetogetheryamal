﻿using System;
using System.Linq;
using System.Reflection;
using WeTogetherYamal.Constants;

namespace WeTogetherYamal.Infrastructure.Data.Extensions
{
    public static class EnumExtensions
    {
        public static string GetStringEnumValue<TEnum, TValueType>(TEnum enumValue)
            where TEnum : struct, Enum
            where TValueType : class
        {
            var type = typeof(TValueType)
                .GetField(
                    enumValue.ToString(),
                    System.Reflection.BindingFlags.Public | System.Reflection.BindingFlags.Static);

            return type.GetRawConstantValue().ToString();
        }
    }

    public static class UserRoleExtensions
    {
        public static string GetUserRoleId(this UserRolesEnum userRole)
        {
            return EnumExtensions.GetStringEnumValue<UserRolesEnum, UserRoleIds>(userRole);
        }

        public static UserRolesEnum GetUserRoleById(this Guid userRoleId)
        {
            var userRolesType = typeof(UserRoleIds);

            var properties = userRolesType
                .GetFields(BindingFlags.Public | BindingFlags.Static)
                .Select(x => new
                {
                    name = x.Name,
                    value = Guid.Parse(x.GetValue(null).ToString())
                }).ToList();

            var name = properties.FirstOrDefault(x => x.value == userRoleId)?.name;

            return Enum.Parse<UserRolesEnum>(name);
        }
    }
}
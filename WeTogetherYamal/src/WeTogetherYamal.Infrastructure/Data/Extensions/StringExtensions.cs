﻿namespace WeTogetherYamal.Infrastructure.Data.Extensions
{
    public static class StringExtensions
    {
        public static Guid? GetGuid(this string src)
        {
            if (!string.IsNullOrWhiteSpace(src) && Guid.TryParse(src,out Guid result))
            {
                return result;
            }

            return default(Guid?);
        }

        public static List<Guid> GetGuidList(this string src, string separator)
        {
            var resultList = new List<Guid>();
            {
                if (!string.IsNullOrWhiteSpace(src))
                    foreach (var guidString in src.Split(separator))
                    {
                        if (Guid.TryParse(guidString, out var guid))
                        {
                            resultList.Add(guid);
                        }
                    }
            }

            return resultList;
        }
    }
}
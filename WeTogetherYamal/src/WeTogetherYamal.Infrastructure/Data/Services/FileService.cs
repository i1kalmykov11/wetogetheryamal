﻿using Flurl;
using Microsoft.AspNetCore.Http;
using SixLabors.ImageSharp;
using WeTogetherYamal.Constants;
using WeTogetherYamal.Infrastructure.Configuration.SettingsModels;
using WeTogetherYamal.SharedKernel.Interfaces.Repository;
using WeTogetherYamal.SharedKernel.Interfaces.Services;
using WeTogetherYamal.SharedKernel.Models.Dto;
using File = WeTogetherYamal.SharedKernel.Entities.File;

namespace WeTogetherYamal.Infrastructure.Data.Services;

public class FileService : IFileService
{
    private readonly IFileRepository _fileRepository;
    private readonly FileSettings _fileSettings;
    private readonly IUnitOfWork _unitOfWork;

    public FileService(IFileRepository fileRepository, FileSettings fileSettings, IUnitOfWork unitOfWork)
    {
        _fileRepository = fileRepository ?? throw new ArgumentNullException(nameof(fileRepository));
        _fileSettings = fileSettings ?? throw new ArgumentNullException(nameof(fileSettings));
        _unitOfWork = unitOfWork ?? throw new ArgumentNullException(nameof(unitOfWork));
    }

    public async Task<File> CreateFile(CreateFileDto model)
    {
        var fileType = GetFileType(model.EventTypeId, model.CityTypeId, model.EventRequestId).ToString();
        string uploadsFolder = Path.Combine(_fileSettings.ImageStorage, fileType);
        string uniqueFileName = Guid.NewGuid().ToString() + "_" + model.File.FileName;
        string filePath = Path.Combine(uploadsFolder, uniqueFileName);
        using var imageLoad = Image.Load(model.File.OpenReadStream());
        imageLoad.Save(filePath);

        var file = new File()
        {
            CreatedAt = DateTimeOffset.UtcNow,
            FileName = model.File.FileName,
            FilePath = $"/img/{fileType}/{uniqueFileName}"
        };

        _fileRepository.Add(file);
        await _unitOfWork.SaveChangesAsync();
        return file;
    }

    public async Task<List<File>> CreateFilesAsync(CreateFilesDto createFileModel)
    {
        List<File> fileList = new List<File>();
        foreach (var formFile in createFileModel.Files)
        {
            var fileType = GetFileType(createFileModel.EventTypeId, createFileModel.CityTypeId,
                createFileModel.EventRequestId).ToString();
            string uploadsFolder = Path.Combine(_fileSettings.ImageStorage, fileType);
            string uniqueFileName = Guid.NewGuid().ToString() + "_" + formFile.FileName;
            string filePath = Path.Combine(uploadsFolder, uniqueFileName);
            using var imageLoad = Image.Load(formFile.OpenReadStream());
            imageLoad.Save(filePath);

            var file = new File()
            {
                CreatedAt = DateTimeOffset.UtcNow,
                FileName = formFile.FileName,
                FilePath = $"/img/{fileType}/{uniqueFileName}",
                EventRequestId = createFileModel.EventRequestId,
                CityTypeId = createFileModel.CityTypeId,
                EventTypeId = createFileModel.EventTypeId
            };

            fileList.Add(file);
        }
        
        _fileRepository.AddRange(fileList);
        await _unitOfWork.SaveChangesAsync();
        return fileList;
    }

    private FileTypeEnum GetFileType(EventTypeEnum? eventType, CityEnum? cityType, Guid? eventRequestId)
    {
        if (eventType.HasValue)
        {
            return FileTypeEnum.EventAlbom;
        }
        if (eventRequestId.HasValue)
        {
            return FileTypeEnum.EventRequestAlbom;
        }
        if (cityType.HasValue)
        {
            return FileTypeEnum.CityAlbom;
        }

        return FileTypeEnum.Other;
    }
}
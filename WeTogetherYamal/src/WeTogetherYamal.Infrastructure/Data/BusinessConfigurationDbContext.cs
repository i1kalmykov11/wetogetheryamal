﻿using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Identity.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore;
using WeTogetherYamal.Infrastructure.Data.Mappings;
using WeTogetherYamal.SharedKernel.Entities;
using File = WeTogetherYamal.SharedKernel.Entities.File;

namespace WeTogetherYamal.Infrastructure.Data
{
    public class BusinessConfigurationDbContext : IdentityDbContext<ApplicationUser, ApplicationRole, Guid,
        IdentityUserClaim<Guid>, ApplicationUserRole,
        IdentityUserLogin<Guid>, IdentityRoleClaim<Guid>, IdentityUserToken<Guid>>
    {
        public BusinessConfigurationDbContext(DbContextOptions<BusinessConfigurationDbContext> options)
            : base(options)
        {
        }

        public DbSet<ApplicationUser> ApplicationUsers { get; set; }
        public DbSet<CityType> CityTypes { get; set; }
        public DbSet<EventRequest> EventRequests { get; set; }
        public DbSet<EventType> EventTypes { get; set; }
        public DbSet<File> Files { get; set; }

        protected override void OnModelCreating(ModelBuilder builder)
        {
            base.OnModelCreating(builder);

            builder.ApplyConfiguration(new ApplicationRoleMap());
            builder.ApplyConfiguration(new ApplicationUserRoleMap());
            builder.ApplyConfiguration(new ApplicationUserMap());

            builder.ApplyConfiguration(new CityTypeMap());
            builder.ApplyConfiguration(new EventRequestMap());
            builder.ApplyConfiguration(new EventTypeMap());
            builder.ApplyConfiguration(new FileMap());
        }
    }
}
﻿using WeTogetherYamal.SharedKernel.Models.EventRequest;
using WeTogetherYamal.SharedKernel.Models.Paging;
using WeTogetherYamal.SharedKernel.Models.Search;

namespace WeTogetherYamal.Core.Interfaces;

public interface ICoreEventRequestService
{
    Task CreateEventRequestAsync(CreateEventRequestViewModel model);

    Task<EventRecord[]> GetEventRequestListAsync(PagingModel pagingModel,
        EventRequestSearchModel searchModel);
}
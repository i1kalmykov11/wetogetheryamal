﻿using AutoMapper;
using WeTogetherYamal.Core.Interfaces;
using WeTogetherYamal.SharedKernel.Entities;
using WeTogetherYamal.SharedKernel.Interfaces.Repository;
using WeTogetherYamal.SharedKernel.Interfaces.Services;
using WeTogetherYamal.SharedKernel.Models.Dto;
using WeTogetherYamal.SharedKernel.Models.EventRequest;
using WeTogetherYamal.SharedKernel.Models.Paging;
using WeTogetherYamal.SharedKernel.Models.Search;

namespace WeTogetherYamal.Core.Services;

public class CoreEventRequestService : ICoreEventRequestService
{
    private readonly IEventRequestRepository _eventRequestRepository;
    private readonly IUnitOfWork _unitOfWork;
    private readonly IMapper _mapper;
    private readonly IFileService _fileService;

    public CoreEventRequestService(IEventRequestRepository eventRequestRepository, IUnitOfWork unitOfWork,
        IMapper mapper, IFileService fileService)
    {
        _eventRequestRepository =
            eventRequestRepository ?? throw new ArgumentNullException(nameof(eventRequestRepository));
        _unitOfWork = unitOfWork ?? throw new ArgumentNullException(nameof(unitOfWork));
        _mapper = mapper ?? throw new ArgumentNullException(nameof(mapper));
        _fileService = fileService ?? throw new ArgumentNullException(nameof(fileService));
    }

    public async Task CreateEventRequestAsync(CreateEventRequestViewModel model)
    {
        var eventRequest = _mapper.Map<EventRequest>(model);
        _eventRequestRepository.Add(eventRequest);
        await _unitOfWork.SaveChangesAsync();

        await _fileService.CreateFilesAsync(new CreateFilesDto()
        {
            Files = model.Files,
            EventRequestId = eventRequest.Id
        });
    }

    public async Task<EventRecord[]> GetEventRequestListAsync(PagingModel pagingModel,
        EventRequestSearchModel searchModel)
    {
        return await _eventRequestRepository.GetSearchedAndPaginatedEventRequestList(searchModel, pagingModel);
    }
}
﻿using AutoMapper;
using WeTogetherYamal.SharedKernel.Entities;
using WeTogetherYamal.SharedKernel.Models.EventRequest;

namespace WeTogetherYamal.Core.MapperProfiles
{
    public class MapperProfile : Profile
    {
        public MapperProfile()
        {
            CreateMap<CreateEventRequestViewModel, EventRequest>()
                .ForMember(dest => dest.Id, opt => opt.Ignore())
                .ForMember(dest => dest.Images, opt => opt.Ignore())
                .ForMember(dest => dest.Address, opt => opt.MapFrom(opt => opt.Address))
                .ForMember(dest => dest.EventTypeId, opt => opt.MapFrom(opt => opt.EventTypeId))
                .ForMember(dest => dest.Email, opt => opt.MapFrom(opt => opt.Email))
                .ForMember(dest => dest.BiographyData, opt => opt.MapFrom(opt => opt.BiographyData))
                .ForMember(dest => dest.CombatVeteranPath, opt => opt.MapFrom(opt => opt.CombatVeteranPath))
                .ForMember(dest => dest.PhoneNumber, opt => opt.MapFrom(opt => opt.PhoneNumber))
                .ForMember(dest => dest.CityId, opt => opt.MapFrom(opt => opt.CityId))
                .ForMember(dest => dest.FIO, opt => opt.MapFrom(opt => opt.FIO))
                .ForMember(dest => dest.BirthDate, opt => opt.MapFrom(opt => opt.BirthDate));
        }
    }
}

﻿using System;
using WeTogetherYamal.Constants;
using WeTogetherYamal.SharedKernel.Interfaces;

namespace WeTogetherYamal.SharedKernel.Models
{
    public class UserSecurityContext : IUserSecurityContext
    {
        public string Token { get; set; }
        public UserRolesEnum Role { get; set; }
        public Guid UserId { get; set; }
        public string Email { get; set; }
    }
}

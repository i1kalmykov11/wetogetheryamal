﻿using Microsoft.AspNetCore.Http;
using WeTogetherYamal.Constants;

namespace WeTogetherYamal.SharedKernel.Models.Dto;

public class CreateFilesDto
{
    public ICollection<IFormFile> Files { get; set; }
    public EventTypeEnum? EventTypeId { get; set; }
    public CityEnum? CityTypeId { get; set; }
    public Guid? EventRequestId { get; set; }
}
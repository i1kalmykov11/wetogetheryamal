﻿using WeTogetherYamal.Constants;
using Microsoft.AspNetCore.Http;

namespace WeTogetherYamal.SharedKernel.Models.Dto;

public class CreateFileDto
{
    public IFormFile File { get; set; }
    public EventTypeEnum? EventTypeId { get; set; }
    public CityEnum? CityTypeId { get; set; }
    public Guid? EventRequestId { get; set; }
}
namespace WeTogetherYamal.SharedKernel.Models.Paging;

public class PagingModel
{
    public PagingModel()
    {
    }

    public PagingModel(string order, string orderBy, int? limit = 100, int? offset = 0)
    {
        Order = order;
        OrderBy = orderBy;
        Limit = limit ?? 100;
        Offset = offset ?? 0;
    }

    public virtual string Order { get; set; }

    public virtual string OrderBy { get; set; }

    public virtual int? Limit { get; set; }

    public virtual int? Offset { get; set; }
}
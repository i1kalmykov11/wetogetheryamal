﻿using WeTogetherYamal.Constants;

namespace WeTogetherYamal.SharedKernel.Models.Search;

public class SearchModel
{
    public virtual string[] SearchProperties { get; set; }

    public virtual string SearchValue { get; set; }
}
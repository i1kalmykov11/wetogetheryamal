﻿using WeTogetherYamal.Constants;

namespace WeTogetherYamal.SharedKernel.Models.Search;

public class EventRequestSearchModel : SearchModel
{
    public CityEnum? City { get; set; }
    public EventTypeEnum? EventType { get; set; }
}
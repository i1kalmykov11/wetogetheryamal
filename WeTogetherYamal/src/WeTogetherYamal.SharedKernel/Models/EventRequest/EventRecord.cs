﻿using WeTogetherYamal.Constants;

namespace WeTogetherYamal.SharedKernel.Models.EventRequest;

public class EventRecord
{
    public string FIO { get; set; }

    public DateTimeOffset BirthDate { get; set; }

    public string Address { get; set; }

    public string Email { get; set; }

    public string PhoneNumber { get; set; }

    public string BiographyData { get; set; }

    public string CombatVeteranPath { get; set; }

    public string[] FilePathes { get; set; }

    public CityEnum CityEnum { get; set; }

    public EventTypeEnum EventEnum { get; set; }
}
﻿using WeTogetherYamal.Constants;
using WeTogetherYamal.SharedKernel.Models.Paging;
using WeTogetherYamal.SharedKernel.Models.Search;

namespace WeTogetherYamal.SharedKernel.Models.EventRequest;

public class GetEventRequestListViewModel
{
    public PagingModel PagingModel { get; set; }
    public EventRequestSearchModel SearchModel { get; set; }
    public EventRecord[] EventRecords { get; set; }
}
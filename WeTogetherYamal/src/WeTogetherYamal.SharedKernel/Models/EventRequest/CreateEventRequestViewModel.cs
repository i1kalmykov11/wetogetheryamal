﻿using Microsoft.AspNetCore.Http;
using WeTogetherYamal.Constants;
using WeTogetherYamal.SharedKernel.Models.Dto;

namespace WeTogetherYamal.SharedKernel.Models.EventRequest;

public class CreateEventRequestViewModel
{
    public bool IsAcceptedConditions { get; set; }

    public string FIO { get; set; }

    public DateTimeOffset? BirthDate { get; set; }

    public string Address { get; set; }

    public string Email { get; set; }

    public string PhoneNumber { get; set; }

    public string BiographyData { get; set; }

    public string CombatVeteranPath { get; set; }

    public ICollection<IFormFile> Files { get; set; }

    public CityEnum CityId { get; set; }

    public EventTypeEnum EventTypeId { get; set; }

    public string ReturnUrl { get; set; }
}
﻿using WeTogetherYamal.Constants;
using WeTogetherYamal.SharedKernel.Interfaces;

namespace WeTogetherYamal.SharedKernel.Entities;

public class CityType : IEntityEnumType<CityEnum>
{
    public CityEnum Id { get; set; }
    public string Name { get; set; }

    public virtual ICollection<File> Images { get; set; }
    public virtual ICollection<EventRequest> EventRequests { get; set; }
}
﻿using WeTogetherYamal.Constants;

namespace WeTogetherYamal.SharedKernel.Entities;

public class File
{
    public Guid Id { get; set; }

    public string FileName { get; set; }

    public string FilePath { get; set; }

    public string OriginalFileName { get; set; }

    public Guid? ThumbFileId { get; set; }

    public DateTimeOffset CreatedAt { get; set; }

    public EventTypeEnum? EventTypeId { get; set; }
    public CityEnum? CityTypeId { get; set; }

    public Guid? EventRequestId { get; set; }

    public virtual File MainFile { get; set; }
    public virtual File ThumbFile { get; set; }
    public virtual EventRequest EventRequest { get; set; }
    public virtual EventType EventType { get; set; }
    public virtual CityType CityType { get; set; }
}
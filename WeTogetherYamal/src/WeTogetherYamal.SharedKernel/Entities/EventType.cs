﻿using WeTogetherYamal.Constants;
using WeTogetherYamal.SharedKernel.Interfaces;

namespace WeTogetherYamal.SharedKernel.Entities;

public class EventType : IEntityEnumType<EventTypeEnum>
{
    public EventTypeEnum Id { get; set; }
    public string Name { get; set; }
    public string Description { get; set; }

    public virtual File Logo { get; set; }
    public virtual ICollection<EventRequest> EventRequests { get; set; }
}

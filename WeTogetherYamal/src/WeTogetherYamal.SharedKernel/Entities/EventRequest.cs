﻿using WeTogetherYamal.Constants;
using WeTogetherYamal.SharedKernel.Interfaces;

namespace WeTogetherYamal.SharedKernel.Entities;

public class EventRequest : IOrder.IGuidId
{
    public Guid Id { get; set; }

    public string FIO { get; set; }

    public DateTimeOffset BirthDate { get; set; }

    public string Address { get; set; }

    public string Email { get; set; }

    public string PhoneNumber { get; set; }

    public string BiographyData { get; set; }

    public string CombatVeteranPath { get; set; }

    public CityEnum CityId { get; set; }

    public EventTypeEnum EventTypeId { get; set; }

    public virtual CityType CityType { get; set; }

    public virtual EventType EventType { get; set; }

    public virtual ICollection<File> Images { get; set; }
}
﻿using System;

namespace WeTogetherYamal.SharedKernel.Interfaces
{
    public interface IOrder
    {
        public interface IOrder
        {
            public int Order { get; set; }
        }

        public interface IIdentifier<TType> where TType : struct
        {
            public TType Id { get; set; }
        }

        public interface IIntId : IIdentifier<int>
        {
        }

        public interface IGuidId : IIdentifier<Guid>
        {
        }

        public interface ICreatedBy
        {
            public DateTimeOffset CreatedAt { get; set; }

            public Guid CreatedBy { get; set; }
        }
    }
}
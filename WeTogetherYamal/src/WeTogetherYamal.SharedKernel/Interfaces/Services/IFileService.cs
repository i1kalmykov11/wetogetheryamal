﻿using WeTogetherYamal.SharedKernel.Models.Dto;
using File = WeTogetherYamal.SharedKernel.Entities.File;

namespace WeTogetherYamal.SharedKernel.Interfaces.Services
{
    public interface IFileService
    {
        Task<File> CreateFile(CreateFileDto model);
        Task<List<File>> CreateFilesAsync(CreateFilesDto createFileModel);
    }
}

﻿namespace WeTogetherYamal.SharedKernel.Interfaces;

public interface IEntityEnumType<TEnumType> where TEnumType : Enum
{
    public TEnumType Id { get; set; }

    public string Name { get; set; }
}
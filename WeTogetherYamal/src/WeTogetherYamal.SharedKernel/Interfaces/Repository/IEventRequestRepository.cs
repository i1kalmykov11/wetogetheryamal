﻿using WeTogetherYamal.SharedKernel.Entities;
using WeTogetherYamal.SharedKernel.Models.EventRequest;
using WeTogetherYamal.SharedKernel.Models.Paging;
using WeTogetherYamal.SharedKernel.Models.Search;

namespace WeTogetherYamal.SharedKernel.Interfaces.Repository;

public interface IEventRequestRepository : IRepository<EventRequest>
{
    Task<EventRecord[]> GetSearchedAndPaginatedEventRequestList(EventRequestSearchModel searchModel,
        PagingModel pagingModel);
}
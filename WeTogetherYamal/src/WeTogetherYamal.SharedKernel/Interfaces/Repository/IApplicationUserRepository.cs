﻿using WeTogetherYamal.SharedKernel.Entities;

namespace WeTogetherYamal.SharedKernel.Interfaces.Repository
{
    public interface IApplicationUserRepository : IRepository<ApplicationUser>
    {
    }
}
﻿using System.Linq.Expressions;

namespace WeTogetherYamal.SharedKernel.Interfaces.Repository
{
    public interface IRepository<TEntity> where TEntity : class
    {
        TEntity GetById(object id);
        IEnumerable<TEntity> Find(Expression<Func<TEntity, bool>> expression);
        void Add(TEntity entity);
        void AddRange(IEnumerable<TEntity> entities);
        void Remove(TEntity entity);
        void RemoveRange(IEnumerable<TEntity> entities);
        void AttachEntity(TEntity entity);
        bool DoesEntityExists<TType>(TType id) where TType : struct;
        bool DoesSetOfEntitiesExist<TType>(ICollection<TType> ids) where TType : struct;
        Task<TEntity> GetByIdAsync(object id);
    }
}
﻿namespace WeTogetherYamal.SharedKernel.Interfaces.Repository
{
    public interface IUnitOfWork
    {
        Task<ITransaction> BeginTransactionAsync();

        int SaveChanges();

        Task<int> SaveChangesAsync();
        bool HasChanges();

        TEntity[] GetModifiedEntities<TEntity>() where TEntity : class;

        TEntity[] GetAddedEntities<TEntity>() where TEntity : class;

        TEntity[] GetChangedEntities<TEntity>() where TEntity : class;
    }
}
﻿using File = WeTogetherYamal.SharedKernel.Entities.File;

namespace WeTogetherYamal.SharedKernel.Interfaces.Repository;

public interface IFileRepository : IRepository<File>
{
    
}
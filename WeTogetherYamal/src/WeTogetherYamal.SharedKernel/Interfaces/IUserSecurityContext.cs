﻿using System;
using WeTogetherYamal.Constants;

namespace WeTogetherYamal.SharedKernel.Interfaces
{
    public interface IUserSecurityContext
    {
        public UserRolesEnum Role { get; set; }
        public Guid UserId { get; set; }
        public string Token { get; set; }
        public string Email { get; set; }
    }
}
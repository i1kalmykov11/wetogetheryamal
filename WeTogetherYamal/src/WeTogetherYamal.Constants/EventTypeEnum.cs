﻿namespace WeTogetherYamal.Constants;

public enum EventTypeEnum
{
    MemoryBookYamal = 1,
    StoryRunsThroughHouse = 2,
    VictoryScarf = 3,
    WarLetters = 4,
    WeRemember = 5
}
﻿namespace WeTogetherYamal.Constants;

public enum CityEnum
{
    Aksarka = 1,
    Gubkinsky = 2,
    Labytnangi = 3,
    Muravlenko = 4,
    Nadym = 5,
    Noyabrsk = 6,
    Salekhard = 7,
    Tazovsky = 8,
    TarkoSale = 9,
    Hanymei = 10
}
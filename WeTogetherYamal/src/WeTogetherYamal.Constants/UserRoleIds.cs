﻿namespace WeTogetherYamal.Constants;

public class UserRoleIds
{
    public static readonly Guid Admin = Guid.Parse(AdminId);

    public const string AdminId = "AFE62804-E037-4758-8261-9BF447D1F579";
}
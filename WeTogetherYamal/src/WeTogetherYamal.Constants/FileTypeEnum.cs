﻿namespace WeTogetherYamal.Constants;

public enum FileTypeEnum
{
    EventAlbom,
    CityAlbom,
    EventRequestAlbom,
    Other
}